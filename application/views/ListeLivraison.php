<!DOCTYPE html>
<html>

<head>
    <title> Liste livraison </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>
<body>
<div class="container-fluid">
<h4> Historique </h4>
        <br>
        <table class="table">
            <tr>
                <th> Client </th>
                <th> Date de livraison </th>
                <th></th>
            </tr>
            <?php for($i = 0; $i < count($livraison) ; $i++) { ?>
                <tr>
                    <td> <?php echo $livraison[$i]['nom'];?> </td>
                    <td> <?php echo $livraison[$i]['dateLivraison'];?> </td>
                    <td> <a href="<?php echo base_url('Livraison/detail?id='.$livraison[$i]['id']);?>"><button class="btn btn-primary"> Voir details  </button></a></td>
                </tr>
            <?php } ?>
        </table>
</div>
</body>