<div class="menu text-light bg-menu p-4">
    <h3 class="text-center mb-4">Comptabilité</h3>
    <ul>
        <li class="clickable grand_menu" id="toogle_procedure_achat" data-toggle="collapse" data-target="#collapse_procedure_achat" aria-expanded="false" aria-controls="collapse_procedure_achat">
        <i class="fa_menu fa fas fa-balance-scale"></i>    
        Procédure d'Achat
        </li>
        <ul class="collapse <?= $collapse[0] ?>" id="collapse_procedure_achat">
            <a class="nav_menu" href="<?= base_url("DemandeAchat") ?>"><li>Demande d'achat</li></a>
            <a class="nav_menu" href="<?= base_url("DemandeProformat") ?>"><li>Demande de proforma</li></a>
            <a class="nav_menu" href="<?= base_url() ?>"><li>Bon de commande</li></a>
            <a class="nav_menu" href="<?= base_url("Reception") ?>"><li>Bon de reception</li></a>
        </ul>
    </ul>
    <ul>
        <li class="clickable grand_menu" id="toogle_procedure_achat" data-toggle="collapse" data-target="#collapse_procedure_vente" aria-expanded="false" aria-controls="collapse_procedure_vente">
        <i class="fa_menu fa fas fa-money"></i>     
        Procédure de Vente
        </li>
        <ul class="collapse <?= $collapse[1] ?>" id="collapse_procedure_vente">
            <a class="nav_menu" href="<?= base_url("Proforma") ?>"><li>Proforma</li></a>
             <a class="nav_menu" href="<?= base_url("Livraison") ?>"><li>Bon de livraison</li></a>
             <a class="nav_menu" href="<?= base_url("Facture") ?>"><li>Facturation</li></a>
        </ul>
    </ul>
    <ul>
        <li class="clickable grand_menu" id="toogle_procedure_achat" data-toggle="collapse" data-target="#collapse_gestion_stock" aria-expanded="false" aria-controls="collapse_gestion_stock">
        <i class="fa_menu fa fas fa-line-chart"></i>     
        Gestion de Stock
        </li>   
        <ul class="collapse <?= $collapse[2] ?>" id="collapse_gestion_stock">
            <a class="nav_menu" href="<?= base_url("Stock") ?>"><li>Entrée</li></a>
            <a class="nav_menu" href="<?= base_url("Stock/sortieStock") ?>"><li>Sortie</li></a>
            <a class="nav_menu" href="<?= base_url("Stock/stockVue") ?>"><li>Etat</li></a>
        </ul>
    </ul>
    <ul>
    <!-- <a class="nav_menu" href="<?= base_url("Immobilisation") ?>"> -->
        <li class="clickable grand_menu" id="toogle_procedure_achat" data-toggle="collapse" data-target="#collapse_immobilisation" aria-expanded="false" aria-controls="collapse_immobilisation">
        <i class="fa_menu fa fas fa-sort-amount-desc"></i>    
        Immobilisation
        </li>   
        <ul class="collapse <?= $collapse[3] ?>" id="collapse_immobilisation">
        <a class="nav_menu" href="<?= base_url("immobilisation/lineaire") ?>"><li>Amortissement linéaire</li></a>
        <a class="nav_menu" href="<?= base_url("immobilisation") ?>"><li>Amortissement degréssif</li></a>
        </ul>
        <!-- </a> -->
    </ul>
</div>