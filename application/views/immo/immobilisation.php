<!DOCTYPE html>
<html>

<head>
    <title> Immobilisation </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body class="back">
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>  
        <div class="col-md mt-4">
            <h4>Amortissement degressif <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <div class="row">
                <div class="col-md">
                    <form action="<?= base_url('/Immobilisation/tableau') ?>" method="POST">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="crud_table">
                                <tr>
                                    <th width="">Date de début</th>
                                    <th width="">Valeur</th>
                                    <th width="">Durée</th>
                                    <th width="">Coefficient</th>
                                </tr>
                                <tr>
                                    <td id="date" class="date" name="date">
                                        <input type="date" name="date">
                                    </td>
                                    <td class="valeur" id="valeur"><input type="number" name="valeur"></td>
                                    <td class="duree" id="duree"><input type="number" name="duree"></td>
                                    <td class="coeff" id="coeff"><input type="text" name="coeff"></td>
                                </tr>
                            </table>
                            <button type="submit" class="btn btn-bg btn-sm">Valider <span class="fa fa-check"></span></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>