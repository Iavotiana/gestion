<!DOCTYPE html>
<html>

<head>
	<title>Tableau d'ammortissement</title>
	<link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.min.js') ?>"></script>
	<script>
		function generatePDF() {
			const element = document.getElementById("pdf");
			html2pdf().from(element).set({
					margin: 0,
					filename: 'Immobilisation.pdf',
					jsPDF: {
						orientation: 'portrait',
						unit: 'in',
						format: 'a4',
						compressPDF: true
					}
				})
				.save();
		}
	</script>
</head>

<body>
	<div class="row">
		<div class="col-md-3">
			<?= $menu ?>
		</div>
		<div class="col-md" style="font-size: small">
			<center>
				<input type="button" class="btn btn-secondary btn-sm mt-3 mb-3" onclick="generatePDF()" value="Imprimer" id="btnPrint">
			</center>
			<div class="shadow">
				<div id="pdf" class="p-4">
					<div id="bonC">
						<center>
							<h4 style="">Tableau d'ammortissement</h4>
						</center>
						<hr>
						<div id="entreprise" style="">
							<b>Date de debut: <?= $date ?></b><br>
							<b>Valeur: <?= number_format($valeur) ?></b><br>
							<b>Duree: <?= $duree ?> an(s)</b><br>
							<b>Coefficient: <?= $coeff ?>%</b><br>
							<b>Type: <?= $type ?></b>
						</div>
						<br>
						<hr>
						<table class="table table-bordered table-hover">
							<thead class="bg-dark-blue">
								<th>Annee</th>
								<th>Valeur</th>
								<th>Date d'util</th>
								<th>Ammortissement cumulé debut</th>
								<th>Taux degressif</th>
								<th>Taux lineaire</th>
								<th>Dotation</th>
								<th>Ammortissement cumulé fin</th>
								<th>Valeur nette</th>
							</thead>
							<tbody>
								<?php for ($i = 0; $i < count($ammort); $i++) { ?>
									<tr>
										<td class="text-left"><?= $ammort[$i]["annee"] ?></td>
										<td class="text-right"><?= number_format($ammort[$i]["va"]) ?></td>
										<td class="text-right"><?= $ammort[$i]["date"] ?></td>
										<td class="text-right"><?= number_format($ammort[$i]["cDebut"]) ?></td>
										<td class="text-right"><?= number_format($ammort[$i]["tauxD"], 2) * 100 ?>%</td>
										<td class="text-right"><?= number_format($ammort[$i]["tauxL"], 2) * 100 ?>%</td>
										<td class="text-right"><?= number_format($ammort[$i]["dotation"]) ?></td>
										<td class="text-right"><?= number_format($ammort[$i]["cFin"]) ?></td>
										<td class="text-right"><?= number_format($ammort[$i]["nette"]) ?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>