<!DOCTYPE html>
<html lang="en">
<head>
	<title>Tableau d'ammortissement</title>
	<link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.min.js') ?>"></script>
	<script>
		function generatePDF() {
			const element = document.getElementById("pdf");
			html2pdf().from(element).set({
					margin: 0,
					filename: 'Immobilisation.pdf',
					jsPDF: {
						orientation: 'portrait',
						unit: 'in',
						format: 'a4',
						compressPDF: true
					}
				})
				.save();
		}
	</script>
</head>
<body>
    <div class="container-fluid">
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md">
        <div class="historique mt-3 p-4 shadow-sm">
                <h5>Details <i class="fa fas fa-history"></i></h5> <hr>
                    <center>
                        Valeur= <?= number_format($amort[0]["valeurI"]) ?><br>
                        Date debut= <?= $amort[0]["dateDebut"] ?><br>
                        Taux= <?= $amort[0]["tauxAmortissement"] ?><br>
                        Nombre d'année= <?= $amort[0]["nombreAnnee"] ?><br>
                    </center>
               
                <table class="table table-hover">
                    <thead class="bg-menu text-white">
                    <tr>
                        <th scope="col">Annee</th>
                        <th scope="col">Valeur</th>
                        <th scope="col">Amortissement Cumulée début</th>
                        <th scope="col">Différence de date</th>
                        <th scope="col">Dotation</th>
                        <th scope="col">Amortissement Cumulée fin</th>
                        <th scope="col">valeur nette</th>
                    </tr>
                    </thead>
                    <tbody> 
                        <?php for ($i = 0; $i < count($amort); $i++) { ?>
                            <tr>
                                <th scope="row"><?= $amort[$i]["annee"] ?></th>
                                <td><?= number_format($amort[$i]["va"]) ?></td>
                                <td><?= number_format($amort[$i]["cumuleDebut"]) ?></td>
                                <td><?= $amort[$i]["dateDiff"] ?></td>
                                <td><?= number_format($amort[$i]["dotation"]) ?></td>
                                <td><?= number_format($amort[$i]["cumuleFin"]) ?></td>
                                <td><?= number_format($amort[$i]["valeurNette"]) ?></td>
                            </tr>
                        <?php } ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>
</html>