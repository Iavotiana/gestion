<!DOCTYPE html>
<html>

<head>
    <title> Creation Proformat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4 class="mb-3 bg text-underline"> Ammortissement lineaire  <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <form class="p-4 shadow-sm" action="<?= base_url("immobilisation/tableauLineaire") ?>" method="POST">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Valeur</label>
                    <div class="col-sm-10">
                        <input name="valeur" type="number" min="0" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Date Debut</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="daty">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Nombre d'année</label>
                    <div class="col-sm-10">
                        <input name="annee" type="number" min="0" class="form-control">
                    </div>
                </div>
                <button type="submit" class="btn btn-bg mb-3">Valider</button>
                <?php if (isset($error)) { ?>
                    <div class="alert alert-danger text-center" role="alert">
                        <?= $error ?>
                    </div>
                <?php } ?>
            </form>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>