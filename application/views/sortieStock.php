<!DOCTYPE html>
<html>

<head>
    <title> Sortie de stock </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body class="back">
    <div class="container p-5 mt-3 shadow bg-white">
        <h4>Sortie produit dans stock</h4>
        <div class="row">
            <div class="col-md">
                <?php include("Produit.modal.php"); ?>
                <div class="table-responsive">
                    <table class="table table-bordered" id="crud_table">
                        <tr>
                            <th width="">
                                Produit
                            </th>
                            <th width="">Restant</th>
                            <th width="">Quantite</th>
                            <th width="">Date</th>
                            <th>-</th>
                        </tr>
                        <?php for ($i = 0; $i < count($stock); $i++) { ?>
                        <tr>
                            <form action="<?= base_url('/stock/insertSort')?>"  method="POST">
                            <td>
                                <?=$stock[$i]['nom']?>
                            </td>
                            <td>
                                <?=$stock[$i]['restant']?>
                            </td>
                            <td class="quantite" id="quantite" ><input type="number" name="quantites"></td>
                            <td id="date" class="date" name="date">
                                <input type="date" name="date">
                            </td>
                            <input type="hidden" name="produits" value="<?=$stock[$i]['idProduit']?>">
                            <td><button type="submit" class="btn btn-danger btn-sm">Valider <span class="fa fa-check"></span></button></td>
                            </form>
                        </tr>
                        <?php } ?>
                    </table>
                    
                </div>
            
            </div>
        </div>
    </div>    
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include("index.js.php"); ?>
</body>

</html>