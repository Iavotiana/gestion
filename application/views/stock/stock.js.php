<script >
	$(document).ready(function() {
        insertEnt();
    });
	function insertEnt() {
        $('#insertEnt').click(function() {
            var produit = [];
            var quantite = [];
            var valeur_unitaire = [];
            var date = [];

            $('.produit').each(function() {
                produit.push($(this).val());
            });

            $('.quantite').each(function() {
                quantite.push($(this).text());
            });

            $('.prix_unitaire').each(function() {
                valeur_unitaire.push($(this).text());
            });

            $('.date').each(function() {
                date.push($(this).text());
            });


            $.ajax({
                url: '<?= base_url("stock/insertEnt") ?>',
                method: 'POST',
                data: {
                    produits: produit,
                    quantites: quantite,
                    valeur_unitaires: valeur_unitaire,
                    date: date
                },
                success: function(data) {
                    alert(data);
                    window.location.reload();
                }
            })
        });
    }
</script>