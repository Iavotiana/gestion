<!DOCTYPE html>
<html>

<head>
	<title>Stock</title>
	<link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
	<div class="row">
		<div class="col-md-3">
			<?= $menu ?>
		</div>
		<div class="col-md mt-4">
		<h4>Etat de Stock</h4>
		<hr>
			<table class="table table-bordered table-hover">
				<thead class="thead-dark">
					<th>Nom produit</th>
					<th>Quantite</th>
					<th>Valeur (Ar)</th>
				</thead>
				<tbody>
					<?php for ($i = 0; $i < count($stock); $i++) { ?>
						<tr>
							<td class="text-left"><?= $stock[$i]["nom"] ?></td>
							<td class="text-right"><?= $stock[$i]["restant"] ?></td>
							<td class="text-right"><?= $stock[$i]["valeur"] ?></td>
				
						</tr>
						<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>