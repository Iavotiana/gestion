<!DOCTYPE html>
<html>

<head>
    <title> Entree de stock </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body class="back">
    <div class="container p-5 mt-3 shadow bg-white">
        <h4>Entree produit dans stock</h4>
        <div class="row">
            <div class="col-md">
                <?php include("Produit.modal.php"); ?>
                <form action="<?= base_url('/stock/insertEnt')?>"  method="POST">
                <div class="table-responsive">
                    <table class="table table-bordered" id="crud_table">
                        <tr>
                            <th width="">
                                <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalProduit">
                                    <b>new </b><span class="fa fa-plus-square"></span>
                                </button>
                                Produit
                            </th>
                            <th width="">Date</th>
                            <th width="">Quantite</th>
                            <th width="">Prix Unitaire</th>
                            <th>-</th>
                        </tr>
                        <tr>
                            <td>
                                <select id="produit" class="produit form-control form-control-sm" name="produits">
                                    <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                        <option value="<?= $produits[$i]['idProduit'] ?>"><?= $produits[$i]["nom"] ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                            <td id="date" class="date" name="date">
                                <input type="date" name="date">
                            </td>
                            <td class="quantite" id="quantite" ><input type="number" name="quantites"></td>
                            <td class="prix_unitaire" id="prix_unitaire" ><input type="number" name="valeur_unitaires"></td>
                            <td></td>
                        </tr>
                    </table>
                    <button type="submit" class="btn btn-success btn-sm">Valider <span class="fa fa-check"></span></button>
                </div>
            </form>
            </div>
        </div>
    </div>    
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include("index.js.php"); ?>
</body>

</html>