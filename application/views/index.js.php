<script>
    $(document).ready(function() {
        addLine();
        removeLine();
        insertBon();
        insertFournisseur();
        insertProduit();
        insertUnite();
    });

    function addLine() {
        var count = 1;
        $("#add").click(function() {
            count += 1;
            var html_code = "<tr id='row" + count + "'>";

            html_code += "<td><select id='produit_"+count+"' class='produit form-control form-control-sm'>";
            <?php for ($i = 0; $i < count($produits); $i++) { ?>
                html_code += "<option value='<?= $produits[$i]['idProduit'] ?>'><?= $produits[$i]["nom"] ?></option>"
            <?php } ?>
            html_code += "</select></td>";

            html_code += "<td><select class='unite form-control form-control-sm'>";
            <?php for ($i = 0; $i < count($unites); $i++) { ?>
                html_code += "<option value='<?= $unites[$i]['idUnite'] ?>''><?= $unites[$i]["code"] ?></option>";
            <?php } ?>
            html_code += "</select></td>";

            html_code += "<td contenteditable='true' class='quantite'></td>";
            html_code += "<td contenteditable='true' class='prix_unitaire'></td>";
            html_code += "<td contenteditable='true' class='reduction'></td>";
            html_code += "<td><button type='button' name='remove' class='btn btn-danger btn-xs remove' btnno='" + count + "'>-</button></td>";
            html_code += "</tr>";
            $('#crud_table').append(html_code);
        });
    }

    function removeLine() {
        $(document).on('click', '.remove', function() {
            var id = $(this).context.attributes["btnno"].value;
            $("#row" + id).remove();
        });
    }

    function insertBon() {
        $('#insert').click(function() {
            var produit = [];
            var unite = [];
            var quantite = [];
            var prix_unitaire = [];
            var reduction = [];

            $('.produit').each(function() {
                produit.push($(this).val());
            });

            $('.unite').each(function() {
                unite.push($(this).val());
            });

            $('.quantite').each(function() {
                quantite.push($(this).text());
            });

            $('.prix_unitaire').each(function() {
                prix_unitaire.push($(this).text());
            });

            $('.reduction').each(function() {
                reduction.push($(this).text());
            });

            var dateBon = $('#dateBon').val();
            var ref = $('#ref').val();
            var fournisseur = $('#fournisseur').val();;
            var remise = $('#remise').val();

            $.ajax({
                url: '<?= base_url("welcome/insert") ?>',
                method: 'POST',
                data: {
                    dateBon: dateBon,
                    ref: ref,
                    fournisseur: fournisseur,
                    remise: remise,
                    produits: produit,
                    unites: unite,
                    quantites: quantite,
                    prix_unitaires: prix_unitaire,
                    reductions: reduction
                },
                success: function(data) {
                    alert(data);
                }
            })
        });
    }

    function insertFournisseur() {
        $("#insertFournisseur").click(function() {
            var nom = $("#nomFournisseur").val();
            var localisation = $("#localisationFournisseur").val();
            $.ajax({
                url: '<?= base_url('Welcome/insertFournisseur'); ?>',
                method: 'POST',
                data: {
                    nom: nom,
                    localisation: localisation
                },
                success: function(data) {
                    alert(data);
                    $("#modalFournisseur").modal('hide');
                    refreshSelectFournisseur();
                }
            });
        });
    }

    function refreshSelectFournisseur() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Welcome/last/Fournisseurs/idFournisseur') ?>',
            async: false,
            success: function(last) {
                var lastJSON = JSON.parse(last);
                var option = '<option value="' + lastJSON.idFournisseur + '">' + lastJSON.nom + '</option>';
                $("#fournisseur").append(option);
    
            }
        });
    }

    function insertProduit() {
        $("#insertProduit").click(function() {
            var idType = $("#typeProduit").val();
            var nom = $("#nomProduit").val();
            $.ajax({
                url: '<?= base_url('Welcome/insertProduit'); ?>',
                method: 'POST',
                data: {
                    idType: idType,
                    nom: nom
                },
                success: function(data) {
                    alert(data);
                    $("#modalProduit").modal('hide');
                    refreshSelectProduit();
                }
            });
        });
    }

    function refreshSelectProduit() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Welcome/last/Produits/idProduit') ?>',
            async: false,
            success: function(last) {
                var lastJSON = JSON.parse(last);
                var option = '<option value="' + lastJSON.idProduit + '">' + lastJSON.nom + '</option>';
                $("#produit").append(option);
            } 
        });
    }

    function insertUnite() {
        $("#insertUnite").click(function() {
            var code = $("#code").val();
            $.ajax({
                url: '<?= base_url('Welcome/insert_unite'); ?>',
                method: 'POST',
                data: {
                    code : code
                },
                success: function(data) {
                    alert(data);
                    $("#modalUnite").modal('hide');
                    refreshSelectUnite();
                }
            });
        });
    }

    function refreshSelectUnite() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Welcome/last/unites/idUnite') ?>',
            async: false,
            success: function(last) {
                var lastJSON = JSON.parse(last);
                var option = '<option value="' + lastJSON.idUnite + '">' + lastJSON.nom + '</option>';
                $("#unite").append(option);
            } 
        });
    }
</script>