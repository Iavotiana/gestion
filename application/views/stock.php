<!DOCTYPE html>
<html>

<head>
	<title>Stock</title>
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.min.js') ?>"></script>
</head>

<body>
	<table class="table table-bordered table-hover">
					<thead class="thead-dark">
						<th>Nom produit</th>
						<th>Quantite</th>
						<th>Prix Unitaire</th>
						<th>Restant</th>
						<th>Date d'entree</th>
					</thead>
					<tbody>
						<?php for ($i = 0; $i < count($stock); $i++) { ?>
							<tr>
								<td class="text-left"><?= $stock[$i]["nom"] ?></td>
								<td class="text-right"><?= $stock[$i]["quantite"] ?></td>
								<td class="text-right"><?= $stock[$i]["valeurUnitaire"] ?></td>
								<td class="text-left"><?= $stock[$i]["restant"] ?></td>
								<td class="text-right"><?= $stock[$i]["dateEntree"] ?></td
							</tr>
						<?php } ?>
					</tbody>
				</table>
	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>