<div class="modal fade" id="modalProduit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau produit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <!-- <form action="<?php echo base_url('Welcome/insertProduit'); ?>" method="post"> -->
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Type</label>
          <div class="col-sm-8">
            <select id="typeProduit" name="idType" class="form-control form-control-sm">
              <?php
              for ($i = 0; $i < count($types); $i++) { ?>
                <option value="<?= $types[$i]['idType'] ?>"><?= $types[$i]["nom"] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-4 col-form-label">Nom</label>
          <div class="col-sm-8">
            <input id="nomProduit" type="text" name="nom" placeholder="nom" class="form-control form-control-sm">
          </div>
        </div>
        <button id="insertProduit" class="btn btn-success btn-sm">Enregistrer</button>
        <!-- </form> -->
      </div>
    </div>
  </div>
</div>