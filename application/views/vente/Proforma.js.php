<script>
    $(document).ready(function() {
        insertProforma();
    });

    function insertProforma() {
        $('#insertPro').click(function() {
            var produit = [];
            var unite = [];
            var quantite = [];
            var prix_unitaire = [];
            var reduction = [];

            $('.produit').each(function() {
                produit.push($(this).val());
            });

            $('.unite').each(function() {
                unite.push($(this).val());
            });

            $('.quantite').each(function() {
                quantite.push($(this).text());
            });

            $('.prix_unitaire').each(function() {
                prix_unitaire.push($(this).text());
            });

            $('.reduction').each(function() {
                reduction.push($(this).text());
            });

            var dateProforma = $('#dateProforma').val();
            var client = $('#client').val();
            var transport = $('#trans').val();
            var datePrevue = $('#datePrevue').val();
            var remise = $('#remise').val();
            $.ajax({
                url: '<?= base_url("Proforma/insert") ?>',
                method: 'POST',
                data: {
                    dateProforma: dateProforma,
                    trans: transport,
                    datePrevue: datePrevue,
                    client: client,
                    remise: remise,
                    produits: produit,
                    unites: unite,
                    quantites: quantite,
                    prix_unitaires: prix_unitaire,
                    reductions: reduction
                },
                success: function(data) {
                    console.log(data);
                    dt = JSON.parse(data);
                    if(dt['error']) {
                        alert(dt['error'].msg);
                    } else {
                        alert(dt['success'].msg);
                        window.location.reload();
                    }
                }
            })
        });
    }
</script>