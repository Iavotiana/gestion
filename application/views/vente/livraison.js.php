<script>
    $(document).ready(function() {
        addLine();
        removeLine();
        insertBon();
        insertFournisseur();
        insertProduit();
        insertUnite();
    });

    function addLine() {
        var count = 1;
        $("#add").click(function() {
            count += 1;
            var html_code = "<tr id='row" + count + "'>";

            html_code += "<td><select id='produit_"+count+"' class='produit form-control form-control-sm'>";
            <?php for ($i = 0; $i < count($produits); $i++) { ?>
                html_code += "<option value='<?= $produits[$i]['idProduit'] ?>'><?= $produits[$i]["nom"] ?></option>"
            <?php } ?>
            html_code += "</select></td>";

            html_code += "<td contenteditable='true' class='quantite'></td>";
            html_code += "<td><button type='button' name='remove' class='btn btn-danger btn-xs remove' btnno='" + count + "'>-</button></td>";
            html_code += "</tr>";
            $('#crud_table').append(html_code);
        });
    }

    function removeLine() {
        $(document).on('click', '.remove', function() {
            var id = $(this).context.attributes["btnno"].value;
            $("#row" + id).remove();
        });
    }

    function insertBon() {
        $('#insert').click(function() {
            var produit = [];
            var quantite = [];

            $('.produit').each(function() {
                produit.push($(this).val());
            });

            $('.quantite').each(function() {
                quantite.push($(this).text());
            });

            var dateBon = $('#dateBon').val();
            var client = $('#client').val();;

            $.ajax({
                url: '<?= base_url("Livraison/insert") ?>',
                method: 'POST',
                data: {
                    dateLivraison: dateBon,
                    client: client,
                    produits: produit,
                    quantites: quantite
                },
                success: function(data) {
                    alert(data);
                    location.reload();
                }
            })
        });
    }

    function insertClient() {
        $("#insertClient").click(function() {
            var nom = $("#nomClient").val();
            var localisation = $("#localisationClient").val();
            $.ajax({
                url: '<?= base_url('Welcome/insertClient'); ?>',
                method: 'POST',
                data: {
                    nom: nom,
                    localisation: localisation
                },
                success: function(data) {
                    alert(data);
                    $("#modalClient").modal('hide');
                    refreshSelectClient();
                }
            });
        });
    }

    function refreshSelectClient() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Welcome/last/clients/idClient') ?>',
            async: false,
            success: function(last) {
                var lastJSON = JSON.parse(last);
                var option = '<option value="' + lastJSON.idClient + '">' + lastJSON.nom + '</option>';
                $("#clients").append(option);
            }
        });
    }

    function insertProduit() {
        $("#insertProduit").click(function() {
            var idType = $("#typeProduit").val();
            var nom = $("#nomProduit").val();
            $.ajax({
                url: '<?= base_url('Welcome/insertProduit'); ?>',
                method: 'POST',
                data: {
                    idType: idType,
                    nom: nom
                },
                success: function(data) {
                    alert(data);
                    $("#modalProduit").modal('hide');
                    refreshSelectProduit();
                }
            });
        });
    }

    function refreshSelectProduit() {
        $.ajax({
            type: "GET",
            url: '<?= base_url('Welcome/last/Produits/idProduit') ?>',
            async: false,
            success: function(last) {
                var lastJSON = JSON.parse(last);
                var option = '<option value="' + lastJSON.idProduit + '">' + lastJSON.nom + '</option>';
                $("#produit").append(option);
            } 
        });
    }
</script>