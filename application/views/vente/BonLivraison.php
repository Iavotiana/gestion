<!DOCTYPE html>
<html>

<head>
    <title> Insertion bon de Livraison </title>
    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>

<body class="back">
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col mt-4">
            <h4>
                <i class="fa fas fa-newspaper-o"></i>
                Bon Livraison
                <i class="fa fas fa-newspaper-o"></i>
            </h4>
            <hr>
            <div class="row">
                <div class="col-md-4 border">
                    <div class="form-group">
                        <label class="col-sm col-form-label">Date</label>
                        <div class="col-sm">
                            <input type="datetime-local" name="dateBon" id="dateBon" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <?php include("client.modal.php"); ?>
                        <label class="col-sm col-form-label">
                            <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalClient">
                                <b>new </b><span class="fa fa-plus-circle"></span>
                            </button>
                            Client
                        </label>
                        <div class="col-sm">
                            <select name="client" id="client" class="form-control form-control-sm">
                                <?php for ($i = 0; $i < count($clients); $i++) { ?>
                                    <option value="<?= $clients[$i]["idClient"] ?>"><?= $clients[$i]["nom"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="crud_table">
                            <tr>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalProduit">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Produit
                                </th>

                                <th width="">Quantite</th>
                                <th>-</th>
                            </tr>
                            <tr>
                                <td>
                                    <select id="produit" class="produit form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                            <option value="<?= $produits[$i]['idProduit'] ?>"><?= $produits[$i]["nom"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td contenteditable="true" class="quantite"></td>
                                <td></td>
                            </tr>
                        </table>
                        <?php include("Produit.modal.php"); ?>
                        <button type="button" name="add" id="add" class="btn btn-dark btn-sm ml-2 mb-2">Ajouter <span class="fa fa-plus"></span></button>
                        <button type="button" id="insert" class="btn btn-bg btn-lg ml-2 mb-2 float-right">Valider <span class="fa fa-check"></span></button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row p-4 mt-2 shadow bg-light" style="font-size:small">
                <h5> Historique <i class="fa fas fa-history"></i> </h5>
                <table class="table table-hover table-bordered">
                    <tr>
                        <th> Client </th>
                        <th> Date de livraison </th>
                        <th></th>
                    </tr>
                    <?php for ($i = 0; $i < count($livraison); $i++) { ?>
                        <tr>
                            <td> <?php echo $livraison[$i]['nom']; ?> </td>
                            <td> <?php echo $livraison[$i]['dateLivraison']; ?> </td>
                            <td> <a href="<?php echo base_url('Livraison/detail?id=' . $livraison[$i]['id']); ?>"><button class="btn btn-secondary btn-sm"> Voir details </button></a></td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include('livraison.js.php') ?>
</body>

</html>