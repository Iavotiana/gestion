<!DOCTYPE html>
<html>

<head>
    <title> Liste livraison </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4>Historique - Détails <i class="fa fas fa-history"></i></h4>
            <br>
            <table class="table w-50">
                <tr class="bg-dark-blue text-white">
                    <th> Produit </th>
                    <th> Quantite </th>
                </tr>
                <?php for ($i = 0; $i < count($details); $i++) { ?>
                    <tr>
                        <td> <?php echo $details[$i]['nom']; ?> </td>
                        <td> <?php echo $details[$i]['quantite']; ?> </td>
                    </tr>
                <?php } ?>
            </table>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>