<?php $tva =  ($facture["ht"] * 20) / 100; ?>
<!DOCTYPE html>
<html>

<head>
	<title>Facture</title>
	<link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.min.js') ?>"></script>
	<script>
		function generatePDF() {
			const element = document.getElementById("bonC");
			html2pdf().from(element).set({
					margin: 0,
					filename: 'Facture.pdf',
					jsPDF: {
						orientation: 'portrait',
						unit: 'in',
						format: 'a4',
						compressPDF: true
					}
				})
				.save();
		}
	</script>
</head>

<body style="font-size:small">
	<div class="row">
		<div class="col-md-3">
			<?= $menu ?>
		</div>
		<div class="col-md">
			<center>
				<input type="button" class="btn btn-secondary btn-sm mt-3 mb-3" onclick="generatePDF()" value="Imprimer" id="btnPrint">
			</center>
			<div class="container shadow">
				<div id="bonC" class="p-4">
					<center>
						<h4>Facture</h4>
					</center>
					<hr>
					<div id="entreprise">
						<b>MC ENTREPRISE</b><br>
						<b>Adresse: Ankorondrano Antananarivo</b><br>
						<b>Tel: +261 34 55 607 88</b>
					</div>
					<br>
					<div class="row">
						<div class="col-md-6">
							<div class="bg-light p-3">
								<div class="row">
									<div class="col-md-5">
										<span>Date:</span><br>
										<span>Bon de commande N°:</span><br>
										<span>Emis par:</span><br>
										<span>Adresse:</span><br>
										<span>Reference:</span>
									</div>
									<div class="col-md">
										<span><i><?= $facture["dateFacture"] ?></i></span><br>
										<span><i><?= $facture["numero"] ?></i></span><br>
										<span><i>MC Entreprise</i></span><br>
										<span><i>Ankorondrano</i></span><br>
										<span><i><?= $facture["reference"] ?></i></span>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 bg-light p-3">
							<b>Destinataire</b><br>
							<span><?= $client["nom"] ?></span><br>
							<span><?= $client["localisation"] ?></span><br>
							<span>Contact</span>
						</div>
					</div>
					<hr>
					<table class="table table-bordered table-hover">
						<thead class="bg-dark-blue text-white">
							<th>Nom produit</th>
							<th>Quantite</th>
							<th>Prix Unitaire</th>
							<th>Unite</th>
							<th>Reduction</th>
							<th style="width:30%;">Montant</th>
						</thead>
						<tbody>
							<?php for ($i = 0; $i < count($details); $i++) { ?>
								<tr>
									<td class="text-left"><?= $details[$i]["produit"] ?></td>
									<td class="text-right"><?= separateur($details[$i]["quantite"]) ?></td>
									<td class="text-right"><?= separateur($details[$i]["prixUnitaire"]) ?></td>
									<td class="text-left"><?= $details[$i]["unite"] ?></td>
									<td class="text-right"><?= $details[$i]["reduction"] ?></td>
									<td class="text-right"><?= separateur($details[$i]["total"]) ?></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

					<table class="table table-bordered bg-light" style="width:30%; margin-right:0; margin-left:auto">
						<tr>
							<th class="bg-dark-blue text-white">Remise</th>
							<td class="text-right"><?= separateur($facture["remise"]) ?></td>
						</tr>
						<tr>
							<th class="bg-dark-blue text-white">Total</th>
							<td class="text-right"><?= separateur($facture["total"]) ?></td>
						</tr>
						<tr>
							<th class="bg-dark-blue text-white">Total HT</th>
							<td class="text-right"><?= separateur($facture["ht"]) ?></td>
						</tr>
						<tr>
							<th class="bg-dark-blue text-white">Total TVA</th>
							<td class="text-right"><?= separateur($tva."") ?></td>
						</tr>
						<tr>
							<th class="bg-dark-blue text-white">Total TTC</th>
							<td class="text-right"><?= separateur($facture["ttc"]) ?></td>
						</tr>
					</table>

					<span>
						Arrêté la présente bon de commande à la somme de:<b class="text-uppercase"><i><?= $lettre ?></i></b><br>
					</span>
					<br>
					<center>
						<span class="" style="margin-right:20px; margin-left:auto">
							SIGNATURE
						</span>
					</center>
				</div>
			</div>
		</div>
	</div>
	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>