<!DOCTYPE html>
<html>

<head>
    <title> Insertion facture </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4>
            <i class="fa fas fa-newspaper-o"></i>
            Insertion facture
            <i class="fa fas fa-newspaper-o"></i>
            </h4>
            <hr>
            <div class="row shadow-sm p-2">
                <div class="col-md-3 bg-light mere border">
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Date</label>
                        <div class="col-sm">
                            <input type="datetime-local" name="dateFacture" id="dateFacture" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Remise </label>
                        <div class="col-sm">
                            <input type="number" name="remise" id="remise" placeholder="pourcentage de remise" class="form-control form-control-sm" />
                        </div>
                    </div>
                    <div class="form-group">
                        <?php include("Client.modal.php"); ?>
                        <label class="col-sm col-form-label font-weight-bold">
                            <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalClient">
                                <b>new </b><span class="fa fa-plus-circle"></span>
                            </button>
                            Client
                        </label>
                        <div class="col-sm">
                            <select name="client" id="client" class="form-control form-control-sm">
                                <?php for ($i = 0; $i < count($clients); $i++) { ?>
                                    <option value="<?= $clients[$i]["idClient"] ?>"><?= $clients[$i]["nom"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Reference </label>
                        <div class="col-sm">
                            <input type="text" name="ref" id="ref" placeholder="votre reference" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
                <div class="col-md fille">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="crud_table">
                            <tr>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalProduit">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Produit
                                </th>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalUnite">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Unite
                                </th>
                                <th width="">Quantite</th>
                                <th width="">Prix Unitaire</th>
                                <th width="">Reduction</th>
                                <th>-</th>
                            </tr>
                            <tr>
                                <td>
                                    <select id="produit" class="produit form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                            <option value="<?= $produits[$i]['idProduit'] ?>"><?= $produits[$i]["nom"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="unite" class="unite form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($unites); $i++) { ?>
                                            <option value="<?= $unites[$i]['idUnite'] ?>"><?= $unites[$i]["code"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td contenteditable="true" class="quantite"></td>
                                <td contenteditable="true" class="prix_unitaire"></td>
                                <td contenteditable="true" class="reduction"></td>
                                <td></td>
                            </tr>
                        </table>
                        <?php include("Produit.modal.php"); ?>
                        <?php include("Unite.modal.php"); ?>
                        <button type="button" name="add" id="add" class="btn btn-dark btn-sm ml-2 mb-2">Ajouter <span class="fa fa-plus"></span></button>
                        <button type="button" id="insert" class="btn btn-bg btn-lg ml-2 mb-2 float-right">Valider <span class="fa fa-check"></span></button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="historique">
                <h5><i class="fa fas fa-history"></i> Historique</h5>
                <ul>
                    <?php for ($i = 0; $i < count($liste); $i++) { ?>
                        <li>
                            <a class="link_list_bon" href="<?= base_url('Facture/getFacture/' . $liste[$i]['idFacture']) ?>">
                                Facture: <span class="text-decoration"><?= $liste[$i]['numero'] ?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include('index.js.php') ?>
</body>

</html>