<!DOCTYPE html>
<html>

<head>
    <title> Insertion bon de commande </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <table class="table">
    <thead>
        <tr>
        <th scope="col">Num</th>
        <th scope="col">Service</th>
        <th scope="col">Produit</th>
        <th scope="col">Quantite</th>
        <th scope="col">Date</th>
        </tr>
    </thead>
    <tbody>
       
        <?php for ($i = 0; $i < count($demandeAchat); $i++) { ?>
            <tr>
            <th scope="row"><?= $demandeAchat[$i]["idDemandeAchat"] ?></th>
            <td><?= $demandeAchat[$i]["services"] ?></td>
            <td><?= $demandeAchat[$i]["produits"] ?></td>
            <td><?= $demandeAchat[$i]["qt"] ?></td>
            <td><?= $demandeAchat[$i]["daty"] ?></td>
            </tr>
        <?php } ?>
        
       
    </tbody>
    </table>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>