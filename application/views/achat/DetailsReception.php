<!DOCTYPE html>
<html>

<head>
    <title> Liste livraison </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>
<body>
<div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
        <h4>Historique - Détails <i class="fa fas fa-history"></i></h4>
            <br>
            <table class="table table-hover">
                <thead class="bg-menu text-white">
                    <tr>
                        <th scope="col"> Produit </th>
                        <th scope="col"> Quantite </th>
                    </tr>
                </thead>
                <tbody>
                    <?php for($i = 0; $i < count($details) ; $i++) { ?>
                        <tr>
                            <td> <?php echo $details[$i]['nom'];?> </td>
                            <td> <?php echo $details[$i]['quantite'];?> </td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
</div>
</body>