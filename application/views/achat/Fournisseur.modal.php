<?php $this->load->helper('url'); ?>
<div class="modal fade" id="modalFournisseur" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Nouveau fournisseur</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Nom</label>
            <div class="col-sm-8">
              <input id="nomFournisseur" type="text" name="nom" placeholder="nom" class="form-control form-control-sm">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-4 col-form-label">Localisation</label>
            <div class="col-sm-8">
              <input id="localisationFournisseur" type="text" name="localisation" placeholder="localisation" class="form-control form-control-sm">
            </div>
          </div>
          <button id="insertFournisseur" class="btn btn-success btn-sm">Enregistrer</button>
      </div>
    </div>
  </div>
</div>