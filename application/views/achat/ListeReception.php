<!DOCTYPE html>
<html>

<head>
    <title> Liste livraison </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>
<body>
<div class="container-fluid mt-2 ml-2 mr-2">
<h4> Historique </h4>
        <br>
        <table class="table">
            <tr>
                <th> Client </th>
                <th> Date de livraison </th>
                <th></th>
            </tr>
            <?php for($i = 0; $i < count($reception) ; $i++) { ?>
                <tr>
                    <td> <?php echo $reception[$i]['nom'];?> </td>
                    <td> <?php echo $reception[$i]['dateReception'];?> </td>
                    <td> <a href="<?php echo base_url('Reception/detail?id='.$reception[$i]['id']);?>"><button class="btn btn-primary"> Voir details  </button></a></td>
                </tr>
            <?php } ?>
        </table>
</div>
</body>