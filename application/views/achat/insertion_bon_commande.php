<!DOCTYPE html>
<html>

<head>
    <title> Insertion bon de commande </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>

<body class="back">
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md p-4">
            <h4>
                <i class="fa fas fa-newspaper-o"></i>
                Bon de Commande
                <i class="fa fas fa-newspaper-o"></i>
            </h4>
            <hr class="trait">
            <div class="row shadow-sm p-2">
                <div class="col-md-3 bg-light mere border">
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Date</label>
                        <div class="col-sm">
                            <input type="datetime-local" name="dateBon" id="dateBon" class="form-control form-control-sm" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Remise </label>
                        <div class="col-sm">
                            <input type="number" name="remise" id="remise" placeholder="pourcentage de remise" class="form-control form-control-sm" />
                        </div>
                    </div>
                    <div class="form-group">
                        <?php include("Fournisseur.modal.php"); ?>
                        <label class="col-sm col-form-label font-weight-bold">
                            <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalFournisseur">
                                <b>new </b><span class="fa fa-plus-circle"></span>
                            </button>
                            Fournisseur
                        </label>
                        <div class="col-sm">
                            <select name="fournisseur" id="fournisseur" class="form-control form-control-sm">
                                <?php for ($i = 0; $i < count($fournisseurs); $i++) { ?>
                                    <option value="<?= $fournisseurs[$i]["idFournisseur"] ?>"><?= $fournisseurs[$i]["nom"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm col-form-label font-weight-bold">Reference </label>
                        <div class="col-sm">
                            <input type="text" name="ref" id="ref" placeholder="votre reference" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
                <div class="col-md fille">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="crud_table">
                            <tr>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalProduit">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Produit
                                </th>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalUnite">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Unite
                                </th>
                                <th width="">Quantite</th>
                                <th width="">Prix Unitaire</th>
                                <th width="">Reduction</th>
                                <th>-</th>
                            </tr>
                            <tr>
                                <td>
                                    <select id="produit" class="produit form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                            <option value="<?= $produits[$i]['idProduit'] ?>"><?= $produits[$i]["nom"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td>
                                    <select id="unite" class="unite form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($unites); $i++) { ?>
                                            <option value="<?= $unites[$i]['idUnite'] ?>"><?= $unites[$i]["code"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td contenteditable="true" class="quantite"></td>
                                <td contenteditable="true" class="prix_unitaire"></td>
                                <td contenteditable="true" class="reduction"></td>
                                <td style="width: 70px" class="text-center"></td>
                            </tr>
                        </table>
                        <?php include("Produit.modal.php"); ?>
                        <?php include("Unite.modal.php"); ?>
                        <button type="button" name="add" id="add" class="btn btn-dark btn-sm ml-2 mb-2">Ajouter <span class="fa fa-plus"></span></button>
                        <button type="button" id="insert" class="btn btn-bg btn-lg ml-2 mb-2 float-right">Valider <span class="fa fa-check"></span></button>
                    </div>
                </div>
            </div>
            <hr class="trait">
            <div class="historique">
                <h5><i class="fa fas fa-history"></i> Historique</h5>
                <ul>
                    <?php for ($i = 0; $i < count($liste); $i++) { ?>
                        <li>
                            <a class="link_list_bon" href="<?= base_url('Bon/BonC/' . $liste[$i]['id']) ?>">
                                Bon: <span class="text-decoration"><?= $liste[$i]['numero'] ?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include('index.js.php') ?>
</body>

</html>