<!DOCTYPE html>
<html>

<head>
    <title> Insertion bon de reception </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/bon_de_commande.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4>
            <i class="fa fas fa-newspaper-o"></i>
            Insertion bon de reception
            <i class="fa fas fa-newspaper-o"></i>
            </h4>
            <hr>
            <div class="row">
                <div class="col-md-5 border">
                    <div class="form-group">
                        <label class="col-sm col-form-label">Date</label>
                        <div class="col-sm">
                            <input type="datetime-local" name="dateReception" id="dateReception" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="form-group">
                        <?php include("Fournisseur.modal.php"); ?>
                        <label class="col-sm col-form-label">
                            <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalFournisseur">
                                <b>new </b><span class="fa fa-plus-circle"></span>
                            </button>
                            Fournisseur
                        </label>
                        <div class="col-sm">
                            <select name="fournisseur" id="fournisseur" class="form-control form-control-sm">
                                <?php for ($i = 0; $i < count($fournisseurs); $i++) { ?>
                                    <option value="<?= $fournisseurs[$i]["idFournisseur"] ?>"><?= $fournisseurs[$i]["nom"] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-md">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="crud_table">
                            <tr>
                                <th width="">
                                    <button type="button" class="btn btn-secondary btn-sm float-right new" data-toggle="modal" data-target="#modalProduit">
                                        <b>new </b><span class="fa fa-plus-square"></span>
                                    </button>
                                    Produit
                                </th>

                                <th width="">Quantite</th>
                                <th style="width: 50px">-</th>
                            </tr>
                            <tr>
                                <td>
                                    <select id="produit" class="produit form-control form-control-sm">
                                        <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                            <option value="<?= $produits[$i]['idProduit'] ?>"><?= $produits[$i]["nom"] ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                                <td contenteditable="true" class="quantite"></td>
                                <td></td>
                            </tr>
                        </table>
                        <?php include("Produit.modal.php"); ?>
                        <button type="button" name="add" id="add" class="btn btn-dark btn-sm ml-2 mb-2">Ajouter <span class="fa fa-plus"></span></button>
                        <button type="button" id="insert" class="btn btn-bg btn-lg ml-2 mb-2 float-right">Valider <span class="fa fa-check"></span></button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="historique">
                <h5><i class="fa fas fa-history"></i> Historique</h5>
                <ul>
                    <?php for ($i = 0; $i < count($liste); $i++) { ?>
                        <li>
                            <a class="link_list_bon" href="<?= base_url('Reception/detail/' . $liste[$i]['id']) ?>">
                                Bon de Reception: #<span class="text-decoration"><?= $liste[$i]['nom'] ?></span>
                                <span class="text-decoration"><?= $liste[$i]['dateReception'] ?></span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>                            
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
    <?php include('reception.js.php') ?>
</body>

</html>