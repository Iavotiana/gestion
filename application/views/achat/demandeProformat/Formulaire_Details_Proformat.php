
<!-- Modal -->
<div class="modal fade" id="exampleModal<?= $i ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Ajouter ce produit dans le proformat <?= $idProformat ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form action="<?= base_url('demandeProformat/insert_details_Proformat') ?>" method="post">
      <input type="hidden" name="idProformat" value="<?= $idProformat ?>" >
      <input type="hidden" name="idProduit" value="<?= $vDemandeProformat[$i]["idProduit"] ?>" >
      <div class="form-group row">
          <label class="col-sm-2 col-form-label">Produit</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" value="<?= $vDemandeProformat[$i]["produits"] ?>" name="produit">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">Quantite</label>
          <div class="col-sm-10">
            <input type="number" class="form-control" value="<?= $vDemandeProformat[$i]["qt"] ?>" placeholder="<?= $vDemandeProformat[$i]["qt"] ?>" name="qt">
          </div>
        </div>
        <div class="form-group row">
          <label class="col-sm-2 col-form-label">date</label>
          <div class="col-sm-10">
            <input type="date" class="form-control" name="daty">
          </div>
        </div>
        <button type="submit" class="btn btn-success">valider</button>
      </form>
      </div>
    </div>
  </div>
</div></td>