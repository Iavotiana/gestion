<!DOCTYPE html>
<html>

<head>
    <title> Creation Proformat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4 class="mb-3 bg text-underline"> Creer un proformat  <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <table class="table table-hover">
                    <thead class="bg-menu text-white">
                    <tr>
                        <th scope="col">Num</th>
                        <th scope="col">Service</th>
                        <th scope="col">Produit</th>
                        <th scope="col">Quantite</th>
                        <th scope="col">Date</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php for ($i = 0; $i < count($demandeAchat); $i++) { ?>
                        <tr>
                            <th scope="row"><?= $demandeAchat[$i]["idDemandeAchat"] ?></th>
                            <td><?= $demandeAchat[$i]["services"] ?></td>
                            <td><?= $demandeAchat[$i]["produits"] ?></td>
                            <td><?= $demandeAchat[$i]["qt"] ?></td>
                            <td><?= $demandeAchat[$i]["daty"] ?></td>
                        </tr>
                    <?php } ?>


                    </tbody>
                </table>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                    Creer un proformat
                </button>
                    
                <a href="<?= base_url('DemandeProformat/insert_proformat_fournisseur')?>"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                   Liste demande proformat
                </button></a>
                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Date du proformat</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    <form action="<?= base_url('demandeProformat/insertProformat') ?>" method="post">
                            <div class="form-group row">
                            <label class="col-sm-2 col-form-label">date</label>
                            <div class="col-sm-10">
                                <input type="date" class="form-control" name="daty">
                            </div>
                            </div>
                            <button type="submit" class="btn btn-success">valider</button>
                        </form>
                    </div>
                    </div>
                </div>
                </div>
            <hr>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>