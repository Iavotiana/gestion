<!DOCTYPE html>
<html>

<head>
    <title> Creation Proformat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4 class="mb-3 bg text-underline"> Creer un proformat  <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <form action="<?= base_url('demandeProformat/insert_proformat_fournisseur') ?>" method="POST">
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Proformat</label>
                <div class="col-sm-10">
                <select class="form-control" name="idProformat">
                    <?php for ($i = 0; $i < count($demande); $i++) { ?>
                        <option value="<?= $demande[$i]["idProformat"] ?>">Numero: <?= $demande[$i]["idProformat"] ?>(le <?= $demande[$i]["daty"] ?>)</option>
                    <?php } ?>
                </select>
                </div>
            </div>
            <div class="form-group row">
                <label  class="col-sm-2 col-form-label">Fournisseur</label>
                <div class="col-sm-10">
                <select class="form-control" name="idFournisseur">
                    <?php for ($i = 0; $i < count($fournisseur); $i++) { ?>
                        <option value="<?= $fournisseur[$i]["idFournisseur"] ?>"><?= $fournisseur[$i]["nom"] ?></option>
                    <?php } ?>
                </select>
                </div>
            </div>
            <button type="submit" class="btn btn-success">Enregistre</button>
            </form>
            <div class="historique mt-3 p-4 shadow-sm">
                <h5>Liste demande Proformat <i class="fa fas fa-history"></i></h5>
                <hr>
                <table class="table">
            <thead>
                <tr>
                <th scope="col">Num</th>
                <th scope="col">Proformat</th>
                <th scope="col">Fournisseur</th>
                <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php for ($i = 0; $i < count($liste); $i++) { ?>
                    <tr>
                        <th scope="row"><?= $liste[$i]["id"] ?></th>
                        <td>numero <?= $liste[$i]["idProformat"] ?> (le  <?= $liste[$i]["daty"] ?>)</td>
                        <td><?= $liste[$i]["nom"] ?> à <?= $liste[$i]["localisation"] ?></td>
                        <td><a href="<?= base_url('demandeProformat/details_Proformat/'.$liste[$i]["idProformat"].'/'.$liste[$i]["idFournisseur"])?>"><button type="button">
                        voire plus de details</button></a></td>
                    </tr>
                <?php } ?>
            </tbody>
            </table>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>