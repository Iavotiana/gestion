<!DOCTYPE html>
<html>

<head>
    <title> Creation Proformat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4 class="mb-3 bg text-underline"> liste des demandes sans reponse  <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <table class="table table-hover">
                    <thead class="bg-menu text-white">
                    <tr>
                        <th scope="col">Produit</th>
                        <th scope="col">Quantite</th>
                        <th scope="col">Date</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php for ($i = 0; $i < count($vDemandeProformat); $i++) { ?>
                            <tr>
                            <td scope="row"><?= $vDemandeProformat[$i]["produits"] ?></td>
                            <td><?= $vDemandeProformat[$i]["qt"] ?></td>
                            <td><?= $vDemandeProformat[$i]["daty"] ?></td>
                            <td><!-- Button trigger modal -->
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?= $i ?>">
                                Ajouter
                                </button>
                                <?php include('Formulaire_Details_Proformat.php') ?>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            <hr>
            <h4 class="mb-3 bg text-underline"> Proformat numero=  <?= $idProformat ?>  <i class="fa fas fa-balance-scale"></i></h4>
            
            <table class="table table-hover">
            <thead class="bg-menu text-white">
            <tr>
                <th scope="col">Produit</th>
                <th scope="col">Quantite</th>
                <th scope="col">Date</th>
            </tr>
            </thead>
            <tbody>

                <?php for ($i = 0; $i < count($proformat); $i++) { ?>
                    <tr>
                    <td scope="row"><?= $proformat[$i]["nom"] ?></td>
                    <td><?= $proformat[$i]["qt"] ?></td>
                    <td><?= $proformat[$i]["daty"] ?></td>
                    </tr>
                <?php } ?>


            </tbody>
        </table>
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal<?= $i ?>">
                Ajouter
            </button>
            <?php include('Formulaire_Details_Proformat2.php') ?>
            <button type="button" class="btn btn-dark" ><a href="<?= base_url('demandeProformat/formFournisseur') ?>">Envoyer à des fournisseur</a></button>
        </div>
    </div>
       
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>