<!DOCTYPE html>
<html>

<head>
    <title> Creation Proformat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
    <script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.js') ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/html2pdf.bundle.min.js') ?>"></script>
</head>
<script>
		function generatePDF() {
			const element = document.getElementById("pdf");
			html2pdf().from(element).set({
					margin: 0,
					filename: 'DemandeProformat.pdf',
					jsPDF: {
						orientation: 'portrait',
						unit: 'in',
						format: 'a4',
						compressPDF: true
					}
				})
				.save();
		}
	</script>
<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>     
        <div  id="pdf" class="col-md mt-4">
            <div class="row">
                <div class="col-md-2">
                     </div>
                <div class="col-md-3"><br><br>
                    <p>LOGO ENTREPRISE</p>
                    <h5>Numero du demande:<?= $proformat[0]["idProformat"] ?></h5>
                    <p> date :<?= $proformat[0]["daty"] ?></p>
                </div>
                <div class="col-md-3"></div>
                <div class="col-md-3"><br><br>
                    <p>Fournisseur:<?= $fournisseur[0]["nom"] ?></p>
                    <p> localisation :<?= $fournisseur[0]["localisation"] ?></p></div>
                <div class="col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8"><br><br>
                    <center><h2>DEMANDE DE PROFORMAT</h2></center><br><br>
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">Produit</th>
                            <th scope="col">Quantite</th>
                            <th scope="col">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for ($i = 0; $i < count($details); $i++) { ?>
                                    <tr>
                                    <td scope="row"><?= $details[$i]["nom"] ?></td>
                                    <td><?= $details[$i]["qt"] ?></td>
                                    <td><?= $details[$i]["daty"] ?></td>
                                    </tr>
                                <?php } ?>
                        </tbody>
                    </table><br><br>
                </div>
                <div class="col-md-2"></div>
            </div>
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-4">
                    Signature le <?= $proformat[0]["daty"] ?>
                </div>
               </div>
        </div>
        </div>
    <div class="row">
        <div class="col-md-5"> </div>  
        <div class="col-md-7">
            <button type="button" class="btn btn-primary" ><a href="<?= base_url('demandeProformat/formFournisseur') ?>">Retour</a></button>
            <input type="button" class="btn btn-secondary btn-sm mt-3 mb-3" onclick="generatePDF()" value="Imprimer" id="btnPrint">   
        </div>
    </div>

    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>

