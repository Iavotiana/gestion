
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
     <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="a<?= base_url('/assets/css/lo.css') ?>">
	<title>Welcome to CodeIgniter</title>
</head>
<body>
	<div class="container">
		
		<div class="row">
			<div class="col-md-4" ></div>
			<div class="col-md-4" >
				<div class="icon_login">
					<span class="fa fa-user" ></span>
				</div>

				<div  class="contener_login">
					<div  class="containt_login">
						<form>
						 <div class="form-group row">
						    <label for="inputEmail3" class="col-sm-4 col-form-label">Email</label>
						    <div class="col-sm-8">
						      <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
						    </div>
						  </div>
						  <div class="form-group row">
						    <label for="inputPassword3" class="col-sm-4 col-form-label">Password</label>
						    <div class="col-sm-8">
						      <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
						    </div>
						  </div>
						  <br><button type="button" class="btn btn-outline-success  btn-sm btn-block">Valider</button>
						</form>
						
					 </div>
				</div>
			</div>
			<div class="col-md-4" ></div>		
		</div>
	</div>
	<script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>
</html>