<!DOCTYPE html>
<html>

<head>
    <title> Demande Achat </title>

    <link href="<?= base_url('/assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?= base_url('/assets/css/font-awesome.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= base_url('/assets/css/index.css') ?>">
</head>

<body>
    <div class="row">
        <div class="col-md-3">
            <?= $menu ?>
        </div>
        <div class="col-md mt-4">
            <h4 class="mb-3 bg text-underline">Demande d' Achat <i class="fa fas fa-balance-scale"></i></h4>
            <hr>
            <form class="p-4 shadow-sm" action="<?= base_url("demandeAchat/insert_demande_Achat") ?>" method="POST">
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Service</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="idService">
                            <?php for ($i = 0; $i < count($services); $i++) { ?>
                                <option value="<?= $services[$i]["idService"] ?>"><?= $services[$i]["nom"] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Produits</label>
                    <div class="col-sm-10">
                        <select class="form-control" name="idProduits">
                            <?php for ($i = 0; $i < count($produits); $i++) { ?>
                                <option value="<?= $produits[$i]["idProduit"] ?>"><?= $produits[$i]["nom"] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Quantite</label>
                    <div class="col-sm-10">
                        <input name="qt" type="number" min="0" class="form-control">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label font-weight-bold">Date</label>
                    <div class="col-sm-10">
                        <input type="date" class="form-control" name="daty">
                    </div>
                </div>
                <button type="submit" class="btn btn-bg mb-3">Valider</button>
                <?php if (isset($error)) { ?>
                    <div class="alert alert-danger text-center" role="alert">
                        <?= $error ?>
                    </div>
                <?php } ?>
            </form>
            <hr>
            <div class="historique mt-3 p-4 shadow-sm">
                <h5>Historique Demande Achat <i class="fa fas fa-history"></i></h5>
                <table class="table table-hover">
                    <thead class="bg-menu text-white">
                        <tr>
                            <th scope="col">Num</th>
                            <th scope="col">Service</th>
                            <th scope="col">Produit</th>
                            <th scope="col">Quantite</th>
                            <th scope="col">Date</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php for ($i = 0; $i < count($demandeAchat); $i++) { ?>
                            <tr>
                                <th scope="row"><?= $demandeAchat[$i]["idDemandeAchat"] ?></th>
                                <td><?= $demandeAchat[$i]["services"] ?></td>
                                <td><?= $demandeAchat[$i]["produits"] ?></td>
                                <td><?= $demandeAchat[$i]["qt"] ?></td>
                                <td><?= $demandeAchat[$i]["daty"] ?></td>
                            </tr>
                        <?php } ?>


                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="<?= base_url('assets/js/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js') ?>"></script>
</body>

</html>