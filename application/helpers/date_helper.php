<?php
defined('BASEPATH') or exit('No direct script access allowed');
function validateDate($date, $format = 'Y-m-d H:i:s')
{
    date_default_timezone_set("Etc/GMT+3");
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

function isValid($date)
	{
		date_default_timezone_set("Etc/GMT+3");
		$dNow = new DateTime();
		$dDate = new DateTime($date);
		$diff = $dNow->diff($dDate);
		if ($diff->format('%R%a') > 30 || $diff->format('%R%a') < -30) return false;
		return true;
	}
if (!function_exists('verifier_date')) {
    function get_date($dateinput)
    {
        try {
            $dateinput = explode("T", $dateinput, 2);
            $str = "%s %s";
            $date = sprintf($str, $dateinput[0], $dateinput[1]);
            $date .= ":00";
            if(validateDate($date)) {
                return $date;
            }
            throw new Exception("Date invalide");
        } catch (Exception $ex) {
            throw $ex;
        }
    }
}
function genererNum($date, $id)
{
    try {
        $date = explode("-", $date);
        $num = $date[0] . '' . $date[1];
        $date = explode(":", $date[2]);
        $num = $num . '' . $date[0] . '' . $date[1];
        return $id.$num;
    } catch (\Exception $ex) {
        throw $ex;
    }
}
