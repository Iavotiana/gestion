<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Livraison extends CI_Controller
{
	public function __construct() {
		parent::__construct();
		$this->load->model("Bon_model");
        $this->load->model('Livraison_model');
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['clients'] = $this->Bon_model->get_data("clients");
		$this->send['types'] = $this->Bon_model->get_data("types");
		$this->send['livraison'] = $this->Livraison_model->findAll();
		$this->menu['collapse'] = ["", "show", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}

    public function index() {	
		$this->load->view('vente/BonLivraison', $this->send);
	}

    public function detail() {
        $idBonLivraison = $this->input->get("id");
        $this->load->model('Livraison_model');
        $send['details'] = $this->Livraison_model->findDetails($idBonLivraison);
		$send['menu'] = $this->send['menu'];
        $this->load->view('vente/DetailsLivraison', $send);
    }

    public function insert()
	{
		try {
			$input = $this->input->post();
			$this->load->model('Livraison_model');
			$this->Livraison_model->insert($input);
			echo json_encode([
				'success' => [
					'msg' => "Insertion réussie",
					'code' => 200,
				],
			]);
		} catch (\Exception $ex) {
			echo json_encode([
				'error' => [
					'msg' => $ex->getMessage(),
					'code' => $ex->getCode(),
				],
			]);
		}
	}

}
?>