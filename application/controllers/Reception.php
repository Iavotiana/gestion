<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);

class Reception extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Bon_model");
		$this->load->model('Reception_model');
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['fournisseurs'] = $this->Bon_model->get_data("fournisseurs");
        $this->send['types'] = $this->Bon_model->get_data("types");
		$menu['collapse'] = ["show", "", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $menu, TRUE);
		$this->send['liste'] = $this->Reception_model->historique();
	}
    public function index()
	{
		$this->load->view('achat/BonReception', $this->send);
	}
    public function insert()
	{
		$input = $this->input->post();
		try {
			$this->Reception_model->insert($input);
			echo json_encode([
				'success' => [
					'msg' => "Insertion réussie",
					'code' => 200,
				],
			]);
		} catch(\Exception $ex) {
			echo json_encode([
				'error' => [
					'msg' => $ex->getMessage(),
					'code' => $ex->getCode(),
				],
			]);
		}
	}
    public function detail($id) {
        $this->load->model('Reception_model');
        $this->send['details'] = $this->Reception_model->findDetails($id);
        $this->load->view('achat/DetailsReception', $this->send);
    }
}
