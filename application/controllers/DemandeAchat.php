<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
class DemandeAchat extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Bon_model");
		$this->load->model("DemandeAchat_model");
		$this->send['services'] = $this->Bon_model->get_data("services");
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['demandeAchat'] = $this->DemandeAchat_model->get_list_demande_achat();
		$this->menu['collapse'] = ["show", "", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}
	public function index()
	{
		$this->load->view('achat/FormDemandeAchat', $this->send);
	}
	public function insert_demande_Achat()
	{
		$this->load->model("DemandeAchat_model");
		try {
			$input = $this->input->post();
			$this->DemandeAchat_model->insert_Demande_Achat($input);
			$this->send['demandeAchat'] = $this->DemandeAchat_model->get_list_demande_achat();	
			$this->load->view('achat/FormDemandeAchat', $this->send);
		} catch (Exception $ex) {
			$this->send['error'] = $ex->getMessage();
			$this->load->view('achat/FormDemandeAchat', $this->send);
		}
		
	}
}
