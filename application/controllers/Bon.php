<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Bon extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Bon_model");
		$this->load->helper('chiffre_helper');
		$menu['collapse'] = ["show", "", "", ""];
		$this->data['menu'] = $this->load->view('menu/menu', $menu, TRUE);
	}
	public function bonC($bon) {
		
		$data["details"] = $this->Bon_model->getDetails($bon);
		$data["bon"] = $this->Bon_model->getBon($bon);
		$data["fournisseur"]= $this->Bon_model->get_Fournisseur($data["bon"]["idFournisseur"]);
		$data["lettre"] = convertir($data["bon"]["ttc"]);
		$data['menu'] = $this->data['menu'];
		$this->load->view('achat/bcommande', $data);
	}

}