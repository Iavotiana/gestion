<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);

class Proforma extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Bon_model");
		$this->load->model("Proforma_model");
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['types'] = $this->Bon_model->get_data("types");
		$this->send['unites'] = $this->Bon_model->get_data("unites");
		$this->send['clients'] = $this->Bon_model->get_data("clients");
		$this->send['liste'] = $this->Proforma_model->get_list_proformat();
		$this->menu['collapse'] = ["", "show", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}
	public function index()
	{
		$this->load->view('vente/proforma', $this->send);
	}

	public function insert()
	{
		try {
			$input = $this->input->post();
			$this->load->model('Proforma_model');
			$this->Proforma_model->insert($input);
			echo json_encode([
				'success' => [
					'msg' => "Insertion réussie",
					'code' => 200,
				],
			]);
		} catch (\Exception $ex) {
			echo json_encode([
				'error' => [
					'msg' => $ex->getMessage(),
					'code' => $ex->getCode(),
				],
			]);
		}
	}
	public function proformavue($pro)
	{
		$this->load->model("Proforma_model");
		$data["details"] = $this->Proforma_model->getDetails($pro);
		$data["pro"] = $this->Proforma_model->getpro($pro);
		$data["client"] = $this->Proforma_model->getClient($data["pro"]["idClient"]);
		$this->load->helper('chiffre_helper');
		$data["lettre"] = convertir($data["pro"]["ttc"]);
		$data['menu'] = $this->send['menu'];
		$this->load->view('vente/ProformaVue', $data);
	}
}
