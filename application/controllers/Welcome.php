<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);

class Welcome extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Bon_model");
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['types'] = $this->Bon_model->get_data("types");
		$this->send['unites'] = $this->Bon_model->get_data("unites");
		$this->send['fournisseurs'] = $this->Bon_model->get_data("fournisseurs");
		$this->send['liste'] = $this->Bon_model->get_list_bon();
		$this->menu['collapse'] = ["show", "", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}
	public function login()
	{
		$this->load->view('login');
	}
	public function index()
	{
		$this->load->view('achat/insertion_bon_commande', $this->send);
	}

	public function insert()
	{
		try {
			$input = $this->input->post();
			$this->Bon_model->insert($input);
			echo json_encode([
				'success' => [
					'msg' => "Insertion réussie",
					'code' => 200,
				],
			]);
		} catch(\Exception $ex) {
			echo json_encode([
				'error' => [
					'msg' => $ex->getMessage(),
					'code' => $ex->getCode(),
				],
			]);
		}
		
	}
	public function insertFournisseur()
	{
		$nom = $this->input->post('nom');
		$localisation = $this->input->post('localisation');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_fournisseur($nom, $localisation);
		echo "Fournisseur enregistré avec succès.";

	}	
	public function insertProduit()
	{
		$idType = $this->input->post('idType');
		$nom = $this->input->post('nom');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_produit($idType, $nom);
		echo "Produit enregistré avec succès.";
	}
	public function insertClient() {
		$nom = $this->input->post('nom');
		$localisation = $this->input->post('localisation');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_client($nom, $localisation);
		echo "Client enregistré avec succès.";
	}	
	public function insert_unite()
	{
		$code = $this->input->post('code');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_unite($code);
		echo "Unite enregistré avec succès.";
	}
	public function insert_type()
	{
		$nom = $this->input->post('nom');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_type($nom);
		$this->load->view("achat/insertion_bon_commande",$this->send);
	}
	public function insert_devise()
	{
		$nom = $this->input->post('nom');
		$this->load->model("Fanilo_model");
		$this->Fanilo_model->insert_devise($nom);
		$this->load->view("achat/insertion_bon_commande",$this->send);
	}
	public function last($table, $column) {
		$last = $this->Bon_model->getLast($table, $column);
		echo json_encode($last);
	}
}
