<?php
defined('BASEPATH') or exit('No direct script access allowed');
error_reporting(0);
class Immobilisation extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Immo_model");
		$this->menu['collapse'] = ["", "", "", "show"];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}
	public function index(){
		$this->load->view('immo/immobilisation', $this->send);
	}
	public function tableau(){
		$input=$this->input->post();
		$date=$input['date'];
		$valeur=$input['valeur'];
		$duree=$input['duree'];
		$coeff=$input['coeff'];
			$amort= $this->Immo_model->degressif($date,$valeur,$duree,$coeff);

		$send['ammort']=$amort;
		$send['valeur']=$valeur;
		$send['date']=$date;
		$send['duree']=$duree;
		$send['coeff']=$coeff;
		$send['type']=$type;
		$send['menu'] = $this->send['menu'];
		$this->load->view('immo/tableau',$send);
	}

	public function lineaire(){
		$this->load->view('immo/immoLineaire',$this->send);
	}
	public function tableauLineaire(){
		$input=$this->input->post();
		$valeur=$input['valeur'];
		$date=$input['daty'];
		$annee=$input['annee'];
		$amort=$this->Immo_model->lineaire($valeur,$date,$annee);
		$this->send['amort']=$amort;
		$this->load->view('immo/tableauLineaire',$this->send);
	}
}
?>