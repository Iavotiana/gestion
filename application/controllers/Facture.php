<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Facture extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Bon_model");
        $this->load->model("Facture_model");
		$this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['types'] = $this->Bon_model->get_data("types");
		$this->send['unites'] = $this->Bon_model->get_data("unites");
		$this->send['clients'] = $this->Bon_model->get_data("clients");
        $this->menu['collapse'] = ["", "show", "", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
        $this->send['liste'] = $this->Facture_model->historique();
    }
    public function index() {
		$this->load->view('vente/insertFacture', $this->send);
    }
    
    public function insert() {
        try {
            $this->load->model("Facture_model");
            $input = $this->input->post();
            $this->Facture_model->insert($input);
            echo json_encode([
                'success' => [
                    'msg' => "Insertion réussie",
                    'code' => 200,
                ],
            ]);
        } catch (\Exception $ex) {
			echo json_encode([
				'error' => [
					'msg' => $ex->getMessage(),
					'code' => $ex->getCode(),
				],
			]);
		}
    }

    public function getFacture($id) {
        $this->load->model("Facture_model");
		$data["details"] = $this->Facture_model->getDetails($id);
		$data["facture"] = $this->Facture_model->getFacture($id);
        $data["client"]= $this->Facture_model->getClient($data["facture"]["idClient"]);
        $this->load->helper('chiffre_helper');
		$data["lettre"] = convertir($data["facture"]["ttc"]);
        $data['menu'] = $this->send['menu'];
		$this->load->view('vente/Facture', $data);
    }

    public function insertClient() {
        $this->load->model("Facture_model");
        $input = $this->input->post();
        $this->Facture_model->insertClient($input);
        echo "Insertion reussie";
    }

}