<?php
defined('BASEPATH') or exit('No direct script access allowed');

class DemandeProformat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("Bon_model");
        $this->load->model("DemandeProformat_model");
        $this->send['produits'] = $this->Bon_model->get_data("produits");
		$this->send['demandeAchat'] = $this->Bon_model->get_data("vdemandeAchat");
        $this->send['vDemandeProformat'] = $this->Bon_model->get_data("vDemandeProformat");
        $this->send['idProformat'] = $this->DemandeProformat_model->getLast_idProformat();
        $this->send['demande'] = $this->Bon_model->get_data("demandeProformat");
		$this->send['fournisseur'] = $this->Bon_model->get_data("fournisseurs");
        $this->menu['collapse'] = ["", "show", "", ""];
		$this->send['liste'] = $this->Bon_model->get_data("vDemandeProfFournisseur");
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
		$this->send['proformat'] = null;
    }
    public function index() {
		$send['demandeAchat'] = $this->Bon_model->get_data("vdemandeAchat");
		$this->load->view("achat/demandeProformat/list_DemandeAchat",$this->send);
    }
    public function insertProformat()
	{
		$daty = $this->input->post('daty');
		$this->DemandeProformat_model->insert_Proformat($daty);
		$this->send['idProformat'] = $this->DemandeProformat_model->getLast_idProformat();
		$this->load->view("achat/demandeProformat/DemandeProformat",$this->send);
	}
    public function insert_details_proformat()
	{
		$idProformat = $this->input->post('idProformat');
		$idProduit = $this->input->post('idProduit');
		$qt = $this->input->post('qt');
		$daty = $this->input->post('daty');
		$this->DemandeProformat_model->insert_Details_Proformat($idProformat, $idProduit,$qt,$daty);
		$this->send['vDemandeProformat'] = $this->Bon_model->get_data("vDemandeProformat");
		$this->send['proformat'] = $this->DemandeProformat_model->get_proformat($idProformat);
		$this->load->view("achat/demandeProformat/DemandeProformat",$this->send);
	}
    public function formFournisseur()
	{
		$this->load->view('achat/demandeProformat/FormProformatFournisseur', $this->send);
	}
    public function ajout_Produit_Proformat($idProformat)
	{
		$this->load->view("achat/demandeProformat/DemandeProformat",$this->send);
	}
	public function insert_proformat_fournisseur()
	{
		$idProformat = $this->input->post('idProformat');
		$idFournisseur = $this->input->post('idFournisseur');
		$this->DemandeProformat_model->insert_Proformat_Fourn($idProformat, $idFournisseur);
		$this->send['liste'] = $this->Bon_model->get_data("vDemandeProfFournisseur");
		$this->load->view('achat/demandeProformat/FormProformatFournisseur', $this->send);	
	}
	public function get_proformat($idProformat)
	{
		$query = $this->db->get_where("vProformat", array("idProformat" => $idProformat));
		return $query->result_array();
	}
	public function get_demande_proformat($idProformat)
	{
		$query = $this->db->get_where("demandeProformat", array("idProformat" => $idProformat));
		return $query->result_array();
	}
	public function get_Fournisseur($idFournisseur)
	{
		$query = $this->db->get_where("fournisseurs", array("idFournisseur" => $idFournisseur));
		return $query->result_array();
	}
	public function details_Proformat($idProformat,$idFournisseur)
	{
		$this->load->model("DemandeAchat_model");
		$this->load->model("Bon_model");
		$send['details'] = $this->DemandeProformat_model->get_proformat($idProformat);
		$send['proformat'] = $this->DemandeProformat_model->get_demande_proformat($idProformat);
		$send['fournisseur'] = $this->DemandeProformat_model->get_Fournisseur($idFournisseur);
		$send['menu']= $this->send['menu'];
		$this->load->view("achat/demandeProformat/affDemProformat",$send);
	}
}
?>
