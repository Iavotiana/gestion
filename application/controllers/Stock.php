<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Stock extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model("Stock_model");
        $this->load->model("Bon_model");
		$this->send['produits'] = $this->Bon_model->get_data("v_produitStock");
		$this->send['types'] = $this->Bon_model->get_data("types");
		$this->send['unites'] = $this->Bon_model->get_data("unites");
		$this->send['fournisseurs'] = $this->Bon_model->get_data("fournisseurs");
		$this->menu['collapse'] = ["", "", "show", ""];
		$this->send['menu'] = $this->load->view('menu/menu', $this->menu, TRUE);
	}
    public function index() {
        $this->load->view('stock/entreeStock', $this->send);
    }
    public function insertEnt(){
        try {
        	$input=$this->input->post();
        	$produits=$input['produits'];
        	$quantites=$input['quantites'];
        	$unit=$input['valeur_unitaires'];
        	$date=$input['date'];
    		$this->Stock_model->entree($produits,$quantites,$unit,$date);
            $this->send['message'] = json_encode([
                'success' => [
                    'msg' => "Insertion réussie",
                    'code' => 200,
                ],
            ]);
        } catch (\Exception $ex) {
            $this->send['message'] = json_encode([
                'error' => [
                    'msg' => $ex->getMessage(),
                    'code' => $ex->getCode(),
                ],
            ]);
        } finally {
            $this->load->view('stock/entreeStock', $this->send);
        }
    }
    public function stockVue(){
    	$send['stock']=$this->Bon_model->get_data('stock');
        $send['menu'] = $this->send['menu'];
    	$this->load->view('stock/stock', $send);
    }
    public function insertSort(){
        try{
        	$input=$this->input->post();
        	$produits=$input['produits'];
        	$quantites=$input['quantites'];
        	// $type=$input['type'];
        	$date=$input['date'];
    		$res = $this->Stock_model->sortie($produits,$quantites,$date);
            $this->send['message'] = json_encode([
                'success' => [
                    'msg' => $res . "Ar",
                    'code' => 200,
                ],
            ]);
        }catch (\Exception $ex) {
            $this->send['message'] = json_encode([
                'error' => [
                    'msg' => $ex->getMessage(),
                    'code' => $ex->getCode(),
                ],
            ]);
        } finally {
            $send['stock'] = $this->Bon_model->get_data('stock');
            $send['menu'] = $this->send['menu'];
            $send['message'] = $this->send['message'];
            $this->load->view('stock/sortieStock', $send);
        }
    }
    public function sortieStock(){
		$send['stock'] = $this->Bon_model->get_data('stock');
        $send['menu'] = $this->send['menu'];
        $this->load->view('stock/sortieStock', $send);
    }
}