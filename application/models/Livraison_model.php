<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Livraison_model extends CI_Model
{
    function __construct()
    {
		$this->load->helper("date_helper");
        $this->load->model("Bon_model");
    }

    public function verifier_bon($data) {
        if(!validateDate($data['dateLivraison'], 'Y-m-d') || !isValid($data['dateLivraison'])) {
            throw new Exception ("Date invalide");
        }
    }

    public function insertBon($data) {
        // $this->verifier_bon($data);
        $input['idClient'] = $data["client"];
        $input['dateLivraison'] = $data["dateLivraison"];
        $this->db->insert('BonLivraison', $input);
    }
    public function isValidDetails($quantites)
	{
        for($i = 0; $i < count($quantites); $i++) {
            if ($quantites <= 0) throw new Exception("Quantite invalide");
        }
	}
    public function insertDetails($data) {
        $produits = $data['produits'];
        $quantites = $data['quantites'];
        $this->isValidDetails($quantites);
        $this->db->select_max('id');
		$bon = $this->db->get('BonLivraison')->row_array()['id'];
		
		$query = "INSERT INTO detailsLivraison(idBonLivraison, idProduit, quantite) VALUES ";
		for($i=0; $i<count($produits); $i++) {
			$query .= "('%s', '%s', '%s')";
			$query = sprintf($query, $bon, $produits[$i], $quantites[$i]);
			if($i != count($produits)-1) {
				$query .= ",";
			}
		}
		$this->db->query($query);
    } 

    public function insert($data) {
        $this->insertBon($data);
        $this->insertDetails($data);
    }

    public function findAll() {
        $str = "SELECT BonLivraison.*, clients.nom from BonLivraison join clients on BonLivraison.idClient = clients.idClient";
        return $this->db->query($str)->result_array();
    }
    public function findDetails($idBonLivraison) {
        $str = "SELECT DetailsLivraison.*, produits.nom from DetailsLivraison join produits on DetailsLivraison.idProduit = produits.idProduit where idBonLivraison = %s";
        $str = sprintf($str, $idBonLivraison);
        // echo $str;
        return $this->db->query($str)->result_array();
    }
}
?>