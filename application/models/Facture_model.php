<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Facture_model extends CI_Model
{
    function __construct()
    {
		$this->load->helper("date_helper");
        $this->load->model("Bon_model");
    }

	public function verifierFacture($data) {
		try {
			$reduction = floatval($data["remise"]);
			if($reduction < 0) throw new Exception("Remise invalide");
			// if(!validateDate($data['dateFacture'], 'Y-m-d')) {
            //     throw new Exception ("Date invalide");
            // }
		} catch (Exception $ex) {
			throw $ex;
		}
	}

	public function verifierDetail($quantite, $prix, $remise) {
		if ($quantite < 1)
			throw new Exception("Quantite invalide");
		if ($prix < 1)
			throw new Exception("Prix invalide");
		if($remise < 0)
			throw new Exception("Reduction invalide");
	}

	public function verifier_details($input) {
		try {
			$produits = $input['produits'];
			$quantites = $input['quantites'];
			$prix_unitaires = $input['prix_unitaires'];
			$reductions = $input['reductions'];
			for ($i = 0; $i < count($produits); $i++) {
				$this->verifierDetail($quantites[$i], $prix_unitaires[$i], $reductions[$i]);
			}
		} catch (Exception $ex) {
			throw $ex;
		}
	}

    public function insertFacture($data)
	{
		$this->verifierFacture($data);
		$input['numero'] = genererNum($data['dateFacture'], rand(0 , 999));
		$input['dateFacture'] = $data['dateFacture'];
		$input['reference'] = $data['ref'];
		$input['idClient'] = $data['client'];
		$input['remise'] = $data['remise'];
		$this->db->insert('facture', $input);
		// return "Insertion reussie";
	}

	public function insertDetails($input) {
		$this->verifier_details($input);
		$produits = $input['produits'];
		$unites = $input['unites'];
		$quantites = $input['quantites'];
		$prix_unitaires = $input['prix_unitaires'];
		$reductions = $input['reductions'];

		$this->db->select_max('idFacture');
		$bon = $this->db->get('facture')->row_array()['idFacture'];
		
		$query = "INSERT INTO detailsFacture VALUES ";
		for($i=0; $i<count($produits); $i++) {
			$query .= "('%s', '%s', '%s', '%s', '%s', '%s')";
			$query = sprintf($query, $bon, $produits[$i], $unites[$i], $quantites[$i], $prix_unitaires[$i], $reductions[$i]);
			if($i != count($produits)-1) {
				$query .= ",";
			}
		}
		$this->db->query($query);
	}

	public function insert($data) {
		$this->insertFacture($data);
		$this->insertDetails($data);
	}

	public function getDetails($id) {
		$query = $this->db->get_where("vDetailsFacture", array("idFacture" => $id));
		return $query->result_array();
	}

	public function getFacture($id) {
		$query = $this->db->get_where("vFacture", array("idFacture" => $id));
		return $query->row_array();
	}

	public function getClient($id) {
		$query = $this->db->get_where("clients", array("idClient" => $id));
		return $query->row_array();
	}

	public function insertClient($input) {
		$this->db->insert('clients', $input);
	}

	public function historique() {
		$sql = "select * from facture order by idFacture desc limit 5";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}