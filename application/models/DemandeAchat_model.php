<?php
defined('BASEPATH') or exit('No direct script access allowed');
class DemandeAchat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('date_helper');
    }
    public function verifier_demande_achat($input) {
        if($input['qt'] < 1)
            throw new Exception("Quantite invalide");
        if(!validateDate($input['daty'], "Y-m-d")) {
            throw new Exception("Date invalide");
        }
    }
    public function insert_Demande_Achat($input)
    {
        $this->verifier_demande_achat($input);
        $request = "insert into demandeAchat (idDemandeAchat, idService, idProduit, qt, daty) values (null,%s,%s,%s,'%s')";
        $request = sprintf($request, $input['idService'], $input['idProduits'], $input['qt'], $input['daty']);
        $this->db->query($request);
    }

    public function get_list_demande_achat() {
        $sql = "SELECT * from vdemandeachat order by iddemandeachat desc limit 5";
		$query = $this->db->query($sql);
		return $query->result_array();
    }

}
