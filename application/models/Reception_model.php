<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Reception_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper("date_helper");
    }

    public function verifier_fille($quantite) {
        if($quantite < 1)
            throw new Exception("Quantite invalide");    
    }
    public function verifier_rec_fille($input) {
        $produits = $input['produits'];
		$quantites = $input['quantites'];
        for($i=0; $i<count($produits); $i++) {
			$this->verifier_fille($quantites[$i]);
		}
    }
    public function insert_mere($input) {
        $date = get_date($input['dateReception']);
        $id = $this->db->get('idRecFin')->row_array()['id'] + 1;
        $sql = "insert into bonreception values ('%s', '%s', '%s')";
        $sql = sprintf($sql, $id, $input['fournisseur'], $date);
        $this->db->query($sql);
        return true;
    }
    public function insert_fille($input) {
        $produits = $input['produits'];
		$quantites = $input['quantites'];
        $bon = $this->db->get('idRecFin')->row_array()['id'];
        $query = "INSERT INTO detailsReception(idBonReception, idProduit, quantite) VALUES ";
		for($i=0; $i<count($produits); $i++) {
			$query .= "('%s', '%s', '%s')";
			$query = sprintf($query, $bon, $produits[$i], $quantites[$i]);
			if($i != count($produits)-1) {
				$query .= ",";
			}
		}
        $this->db->query($query);
    }
    public function insert($input) {
        $this->verifier_rec_fille($input);
        $this->insert_mere($input);
        $this->insert_fille($input);
    }
    public function historique() {
        $str = "Select * from vReception order by id desc limit 5";
        return $this->db->query($str)->result_array();
    }
    public function findDetails($idBonReception) {
        $str = "SELECT DetailsReception.*, produits.nom from DetailsReception join produits on DetailsReception.idProduit = produits.idProduit where idBonReception = %s";
        $str = sprintf($str, $idBonReception);
        return $this->db->query($str)->result_array();
    }
}
?>