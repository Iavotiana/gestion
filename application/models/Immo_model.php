<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Immo_model extends CI_Model
{
    function __construct()
    {
		$this->load->helper("date_helper");
    }
    public function dateUtil($dateU) {
        $str = explode("-", $dateU);
        $dateU = "01-01-".$str[0];
        date_default_timezone_set("Etc/GMT+3");
        $date = date_create($dateU); 
        return date_format($date, "Y-m-d");
    }

    public function degressif($dateU, $valeur, $duree, $coeff) {
        $array = array();
        $duree=(double)$duree;
        $valeur=(double)$valeur;
        $coeff=(double)$coeff;
        $tauxLineaire = 1/$duree;
        $tauxDegressif = $tauxLineaire*$coeff;
        $annee = 1; $i = 0; $cDebut = 0;
        $util = $this->dateUtil($dateU);
        while($annee <= $duree) {
            if($i > 0) $util = date('Y-m-d',strtotime($util . " + 1 year"));
            $tauxLineaire = 1/($duree-$annee + 1);
            $array[$i]["annee"] = $annee;
            $array[$i]["va"] = $valeur;
            $array[$i]["date"] = $util;
            $array[$i]["cDebut"] = $cDebut;
            $array[$i]["tauxD"] = $tauxDegressif;
            $array[$i]["tauxL"] = $tauxLineaire;
            $array[$i]["dotation"] = $valeur*max($tauxLineaire, $tauxDegressif);
            $array[$i]["cFin"] = $cDebut + $array[$i]["dotation"];
            $array[$i]["nette"] = $valeur - $array[$i]["dotation"];
            $valeur = $array[$i]["nette"];
            $cDebut = $array[$i]["cFin"];
            $annee++; $i++;
        }
        return $array;
    }

    public function difference($dateString1, $dateString2) {
        // echo("  fin = ". $dateString1 . " &&  debut = ".$dateString2);
        $date1 = date_create($dateString1);
        $date2 = date_create($dateString2);
        $diff = date_diff($date1, $date2);
        return $diff->format("%a");
    }
    public function getLastDayofYear($dateString) {
        $tabStr = explode("-", $dateString);
        $date = $tabStr[0]."-12-31";
        return $date;
    
    }
    public function getFirstDayofNextYear($dateString) {
        $tabStr = explode("-", $dateString);
        $nextYear = intval($tabStr[0]) + 1;
        $date = strval($nextYear)."-01-01";
        return $date;
    }
    public function getLastDateExercice($nombreAnnee, $dateDebut) {
        $tabStr = explode("-", $dateDebut);
        $year = intval($tabStr[0]) + $nombreAnnee - 1;
        $day = intval($tabStr[2]) - 1;
        $date = strval($year)."-".$tabStr[1]."-".strval($day);
        return $date;
    } 
    public function lineaire($valeurI, $dateDebut, $nombreAnnee) {
        $tab = array();
        $cumuleFin = 0;
        $tauxAmortissement = 100/$nombreAnnee;
        for($i = 0; $i < $nombreAnnee; $i += 1) {
            if($i == 0) {
                $tab[$i]['dateExercice'] =  $dateDebut;
                $tab[$i]['dateDiff'] = $this->difference($this->getLastDayofYear($dateDebut), $dateDebut);
                $tab[$i]['cumuleDebut'] = 0;
            // } else if($i == $nombreAnnee - 1) {
            //     $tab[$i]['dateExercice'] = $this->getLastDateExercice($nombreAnnee, $dateDebut);
            //     $tab[$i]['dateDiff'] = $tab[$nombreAnnee - 2]['dateDiff'] - $tab[0]['dateDiff'];
            //     $tab[$i]['cumuleDebut'] = $tab[$i - 1]['cumuleFin'];
            // } else if($i != 0 && $i != $nombreAnnee - 1) {
            } else {
                $tab[$i]['dateExercice'] = $this->getFirstDayofNextYear($tab[$i - 1]['dateExercice']);
                $tab[$i]['cumuleDebut'] = $tab[$i - 1]['cumuleFin'];
                $tab[$i]['dateDiff'] = 365;
            }
            $tab[$i]['annee'] = $i + 1;
            $tab[$i]['va'] = $valeurI;
            $tab[$i]['dotation'] = $valeurI * ($tauxAmortissement/100) * $tab[$i]['dateDiff']/365;
            $cumuleFin = $cumuleFin + $tab[$i]['dotation'];
            $tab[$i]['cumuleFin'] = $cumuleFin;
            $tab[$i]['valeurNette'] = $valeurI - $tab[$i]['cumuleFin']; 
            $tab[$i]['valeurI'] = $valeurI;
            $tab[$i]['dateDebut'] = $dateDebut;
            $tab[$i]['tauxAmortissement'] = $tauxAmortissement;
            $tab[$i]['nombreAnnee'] = $nombreAnnee;
        }
        $i = $nombreAnnee;
        $tab[$i]['dateExercice'] = $this->getLastDateExercice($nombreAnnee, $dateDebut);
        $tab[$i]['dateDiff'] = $tab[$nombreAnnee - 2]['dateDiff'] - $tab[0]['dateDiff'];
        $tab[$i]['cumuleDebut'] = $tab[$i - 1]['cumuleFin'];
        $tab[$i]['annee'] = $i + 1;
        $tab[$i]['va'] = $valeurI;
        $tab[$i]['dotation'] = $valeurI * ($tauxAmortissement/100) * $tab[$i]['dateDiff']/365;
        $cumuleFin = $cumuleFin + $tab[$i]['dotation'];
        $tab[$i]['cumuleFin'] = $cumuleFin;
        $tab[$i]['valeurNette'] = $valeurI - $tab[$i]['cumuleFin']; 
        $tab[$i]['valeurI'] = $valeurI;
        $tab[$i]['dateDebut'] = $dateDebut;
        $tab[$i]['tauxAmortissement'] = $tauxAmortissement;
        $tab[$i]['nombreAnnee'] = $nombreAnnee;
        return $tab;
    } 
}