<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Stock_model extends CI_Model
{
    function __construct()
    {
        $this->load->model("Bon_model");
    }
    public function verifier_quantite($quantite)
    {
        if ($quantite < 1)
            throw new Exception("Quantite invalide");
    }
    public function verifier_prix($prix)
    {
        if ($prix <= 0)
            throw new Exception("Prix invalide");
    }
    public function fifo($id, $quantite) {
        $query = $this->db->get_where("vFifo", array("idProduit" => $id));
        $data = $query->result_array();
        $qte = 0;
        $valeur = 0;
        try{
            $this->verifier_quantite($quantite);
            for($i = 0; $i < count($data); $i++) {
                $qte += $data[$i]["restant"];
                if($qte <= $quantite) {
                    $array["restant"] = 0;
                    $this->db->update("entree", $array, array('id' => $data[$i]["id"]));
                    $valeur += $data[$i]["valeurUnitaire"] * $data[$i]["restant"];
                }
                if ($quantite == $qte) break;
                if($qte > $quantite) {
                    $array["restant"] = $qte - $quantite ;
                    $this->db->update("entree", $array, array('id' => $data[$i]["id"]));
                    $valeur += $data[$i]["valeurUnitaire"] * ($data[$i]["restant"] -  $array["restant"]);
                    break;
                } 
            }
        }catch (Exception $ex) {
            throw $ex;
        }
        return $valeur/$quantite;
    }

    public function lifo($id, $quantite) {
        $query = $this->db->get_where("vLifo", array("idProduit" => $id));
        $data = $query->result_array();
        $qte = 0;
        $valeur = 0;
        try{
            for($i = 0; $i < count($data); $i++) {
                $qte += $data[$i]["restant"];
                if($qte <= $quantite) {
                    $array["restant"] = 0;
                    $this->db->update("entree", $array, array('id' => $data[$i]["id"]));
                    $valeur += $data[$i]["valeurUnitaire"] * $data[$i]["restant"];
                }
                if ($quantite == $qte) break;
                if($qte > $quantite) {
                    $array["restant"] = $qte - $quantite ;
                    $this->db->update("entree", $array, array('id' => $data[$i]["id"]));
                    $valeur += $data[$i]["valeurUnitaire"] * ($data[$i]["restant"] -  $array["restant"]);
                    break;
                } 
            }
        }catch (Exception $ex) {
            throw $ex;
        }
        return $valeur/$quantite;
    }


    public function entree($idP,$qtt,$unit,$date){
        try{
            $input['idProduit'] = $idP;
            $input['quantite'] = $qtt;
            $input['valeurUnitaire'] = $unit;
            $input['restant'] = $qtt;
            $input['dateEntree'] = $date;
            $this->verifier_quantite($qtt);
            $this->verifier_prix($unit);
            $this->db->insert('entree', $input);
            return "Insertion reussi";
        }catch (Exception $ex) {
            throw $ex;
        }
    } 

    public function getType($produit) {
        $query = $this->db->get_where("stockProduit", array("idProduit" => $produit));
        $data = $query->row_array()["methode"];
        return $data;
    }

    public function sortie($idP,$qtt,$date){
        try{    
            $type = $this->getType($idP);
            $input['idProduit'] = $idP;
            $input['quantite'] = $qtt;
            $this->verifier_quantite($qtt);
            if($type == 1){$input['valeurUnitaire'] = $this->fifo($idP,$qtt);}
            if($type == 2){$input['valeurUnitaire'] = $this->lifo($idP,$qtt);}
            $input['dateSortie'] = $date;
            $this->db->insert('sortie', $input);
            return $input['valeurUnitaire'];
        }catch (Exception $ex) {
            throw $ex;
        }
    } 
}