<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Bon_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('date_helper');
	}
	public function get_data($table)
	{
		$query = $this->db->get($table);
		return $query->result_array();
	}

	public function verifier_bon_mere($input)
	{
		try {
			$reduction = floatval($input['remise']);
			if ($reduction < 0) {
				throw new Exception("Remise invalide");
			}
			// if (strlen($input['ref']) < 1) {
				// throw new Exception("Reférence invalide");
			// }
		} catch (Exception $ex) {
			throw $ex;
		}
	}

	public function verifier_fille($quantite, $prix, $reduction)
	{
		if ($quantite < 1)
			throw new Exception("Quantite invalide");
		if ($prix < 1)
			throw new Exception("Prix invalide");
		if ($reduction < 0)
			throw new Exception("Reduction invalide");
	}

	public function verifier_bon_fille($input)
	{
		$produits = $input['produits'];
		$quantites = $input['quantites'];
		$prix_unitaires = $input['prix_unitaires'];
		$reductions = $input['reductions'];
		for ($i = 0; $i < count($produits); $i++) {
			$this->verifier_fille($quantites[$i], $prix_unitaires[$i], $reductions[$i]);
		}
	}

	public function verifier_bon($input)
	{
		$this->verifier_bon_mere($input);
		$this->verifier_bon_fille($input);
	}

	public function insert_bon_mere($input)
	{
		$input['dateBon'] = get_date($input['dateBon']);
		$id = $this->db->get('idBonFin')->row_array()['id'] + 1;
		$input['numero'] = genererNum($input['dateBon'], $id);
		$sql = "insert into bon values ('%s', '%s', '%s', '%s', '%s', '%s')";
		$sql = sprintf($sql, $id, $input['numero'], $input['dateBon'], $input['ref'], $input['fournisseur'], $input['remise']);
		$this->db->query($sql);
		return true;
	}

	public function insert_bon_fille($input)
	{
		$produits = $input['produits'];
		$unites = $input['unites'];
		$quantites = $input['quantites'];
		$prix_unitaires = $input['prix_unitaires'];
		$reductions = $input['reductions'];
		$bon = $this->db->get('idBonFin')->row_array()['id'];
		$query = "INSERT INTO details VALUES ";
		for ($i = 0; $i < count($produits); $i++) {
			$query .= "('%s', '%s', '%s', '%s', '%s', '%s')";
			$query = sprintf($query, $bon, $produits[$i], $unites[$i], $quantites[$i], $prix_unitaires[$i], $reductions[$i]);
			if ($i != count($produits) - 1) {
				$query .= ",";
			}
		}
		$this->db->query($query);
	}

	public function insert_bon_commande($input)
	{
		$this->insert_bon_mere($input);
		$this->insert_bon_fille($input);
	}

	public function insert($input)
	{
		$this->verifier_bon($input);
		$this->insert_bon_commande($input);
	}

	function get_Fournisseur($id)
	{
		$sql = "SELECT * from fournisseurs where idFournisseur=" . $id;
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	function getLast($table, $column)
	{
		$sql = "SELECT * from %s where %s=(select max(%s) from %s)";
		$sql = sprintf($sql, $table, $column, $column, $table);
		$query = $this->db->query($sql);
		return $query->row_array();
	}
	public function getDetails($bon)
	{
		$query = $this->db->get_where("vDetails", array("bon" => $bon));
		return $query->result_array();
	}

	public function getBon($bon) {
		$query = $this->db->get_where("vBon", array("id" => $bon));	
		return $query->row_array();
	}

	public function get_list_bon()
	{
		$sql = "SELECT * from bon order by id desc limit 5";
		$query = $this->db->query($sql);
		return $query->result_array();
	}
}
