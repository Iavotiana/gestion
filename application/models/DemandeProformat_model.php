<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	class DemandeProformat_model extends CI_Model {
        public function insert_proformat($daty) 
        {
            $request = "insert into demandeproformat (idProformat, daty) values (null,'%s')";
            $request = sprintf($request,$daty);
            $this->db->query($request);
        }
        public function getLast_idProformat() 
        {
            $sql1= " select count(idProformat) as id from demandeProformat";
            $query = $this->db->query($sql1);
            $idProformat= $query->row_array();
            return $idProformat["id"];
        }
        public function insert_Details_Proformat($idProformat, $idProduit,$qt,$daty)
        {
            $input['idDetail'] = null;
            $input['idProformat'] = $idProformat;
            $input['idProduit'] = $idProduit;
            $input['qt'] = $qt;
            $input['daty'] = $daty; 
            $this->db->insert('detailsProformat', $input);
            $this->upDate_DemandeAchat($idProduit);
        }
        public function get_proformat($idProformat)
        {
            $query = $this->db->get_where("vProformat", array("idProformat" => $idProformat));
		    return $query->result_array();
        }
        public function get_demande_proformat($idProformat)
        {
            $query = $this->db->get_where("demandeProformat", array("idProformat" => $idProformat));
		    return $query->result_array();
        }
        public function get_Fournisseur($idFournisseur)
        {
            $query = $this->db->get_where("fournisseurs", array("idFournisseur" => $idFournisseur));
		    return $query->result_array();
        }
        public function upDate_DemandeAchat($idProduit)
        {
            $input['etat'] = "1";      
            $this->db->update('demandeAchat', $input, array('idProduit'=>$idProduit)); 
        }
        public function insert_Proformat_Fourn($idProformat, $idFournisseur)
        {
            $input['id'] = null;
            $input['idProformat'] = $idProformat;
            $input['idFournisseur'] = $idFournisseur;
            $this->db->insert('demandeproffournisseur', $input);
        }
        
    }