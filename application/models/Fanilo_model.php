<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
	class Fanilo_model extends CI_Model {
        public function getNext_id($request) 
        {
            return $this->db->query($request)->result_array()[0]['max'];
        }
        public function insert_fournisseur($nom, $localisation) 
        {
            $request = "INSERT into fournisseurs values(%s, '%s', '%s')";
            $str = "SELECT COALESCE(Max(idFournisseur) + 1, 1) as max from fournisseurs";
            $request = sprintf($request, $this->getNext_id($str), $nom, $localisation);
            $this->db->query($request);
        }
        public function insert_produit($idType, $nom)
        {
            $request = "INSERT into produits values(%s, %s, '%s')";
            $str = "SELECT COALESCE(Max(idProduit) + 1, 1) as max from produits";
            $request = sprintf($request, $this->getNext_id($str), $idType, $nom);
            $this->db->query($request);
        }
        public function insert_client($nom, $localisation) {
            $request = "INSERT into clients(nom, localisation) values('%s', '%s')";
            $request = sprintf($request, $nom, $localisation);
            $this->db->query($request);
        }
        public function insert_unite($code)
        {
            $request = "INSERT into unites values(%s, '%s')";
            $str = "SELECT COALESCE(Max(idUnite) + 1, 1) as max from unites";
            $request = sprintf($request, $this->getNext_id($str), $code);
            $this->db->query($request);
        }
        public function insert_type($nom)
        {
            $request = "INSERT into types values(%s, '%s')";
            $str = "SELECT COALESCE(Max(idtype) + 1, 1) as max from types";
            $request = sprintf($request, $this->db->getNext_id($str), $nom);
            $this->db->query($request);
        }
        public function insert_devise($nom)
        {
            $request = "INSERT into devise values(%s, '%s')";
            $str = "SELECT COALESCE(Max(id) + 1, 1) as max from devise";
            $request = sprintf($request, $this->db->getNext_id($str), $nom);
            $this->db->query($request);
        }
    
    }