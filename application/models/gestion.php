<?php 
function get_Fournisseur() {
	$sql= "SELECT * from fournisseurs";
	$query = $this->db->query($sql);
	$data = array();
	foreach ($query->result_array() as $row) {
		$data[] = $row;
	}
	return $data;
}
function get_All_Produit() {
	$sql= "SELECT * from produits";
	$query = $this->db->query($sql);
	$data = array();
	foreach ($query->result_array() as $row) {
		$data[] = $row;
	}
	return $data;
}
function get_Produit_type($idType) {
	$sql= "SELECT * from produits where idType=%s";
	$sql= sprintf($sql,$idType);
	$query = $this->db->query($sql);
	$data = array();
	foreach ($query->result_array() as $row) {
		$data[] = $row;
	}
	return $data;
}
function get_Unite() {
	$sql= "SELECT * from unites";
	$query = $this->db->query($sql);
	$data = array();
	foreach ($query->result_array() as $row) {
		$data[] = $row;
	}
	return $data;
}
function get_Devise() {
	$sql= "SELECT * from devise";
	$query = $this->db->query($sql);
	$data = array();
	foreach ($query->result_array() as $row) {
		$data[] = $row;
	}
	return $data;
}
?>