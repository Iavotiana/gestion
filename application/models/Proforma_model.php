<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Proforma_model extends CI_Model
{
    public function __construct()
	{
		parent::__construct();
		$this->load->helper('date_helper');
	}
    public function verifier_pro_mere($input)
	{
		try {
			$reduction = floatval($input['remise']);
			if(strlen($input['trans']) < 1) {
                throw new Exception("Transport invalide");
            }
            if(!validateDate($input['datePrevue'], 'Y-m-d')) {
                throw new Exception ("Date prevue invalide");
            }
            if ($reduction < 0) {
				throw new Exception("Remise invalide");
			}
		} catch (Exception $ex) {
			throw $ex;
		}
	}
    public function verifier_fille($quantite, $prix, $reduction)
	{
		if ($quantite < 1)
			throw new Exception("Quantite invalide");
		if ($prix < 1)
			throw new Exception("Prix invalide");
		if($reduction < 0)
			throw new Exception("Reduction invalide");
	}
    public function verifier_pro_fille($input)
	{
		try {
			$produits = $input['produits'];
			$quantites = $input['quantites'];
			$prix_unitaires = $input['prix_unitaires'];
			$reductions = $input['reductions'];
			for ($i = 0; $i < count($produits); $i++) {
				$this->verifier_fille($quantites[$i], $prix_unitaires[$i], $reductions[$i]);
			}
		} catch (Exception $ex) {
			throw $ex;
		}
	}
    public function verifier_pro($input) {
		try {
			$this->verifier_pro_mere($input);
			$this->verifier_pro_fille($input);
		} catch(Exception $ex) {
			throw $ex;
		}
	}
    public function insert_pro_mere($input)
	{
		$input['dateProforma'] = get_date($input['dateProforma']);
		$id = $this->db->get('idProFin')->row_array()['id'] + 1;
		$input['numero'] = genererNum($input['dateProforma'], $id);
		$sql = "insert into proforma values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";
		$sql = sprintf($sql, $id, $input['numero'], $input['dateProforma'], $input['trans'], $input['datePrevue'], $input['client'], $input['remise']);
		$this->db->query($sql);
		return true;
	}

	public function insert_pro_fille($input)
	{
		$produits = $input['produits'];
		$unites = $input['unites'];
		$quantites = $input['quantites'];
		$prix_unitaires = $input['prix_unitaires'];
		$reductions = $input['reductions'];
		$bon = $this->db->get('idProFin')->row_array()['id'];
		$query = "INSERT INTO detailspro VALUES ";
		for ($i = 0; $i < count($produits); $i++) {
			$query .= "('%s', '%s', '%s', '%s', '%s', '%s')";
			$query = sprintf($query, $bon, $produits[$i], $unites[$i], $quantites[$i], $prix_unitaires[$i], $reductions[$i]);
			if ($i != count($produits) - 1) {
				$query .= ",";
			}
		}
		$this->db->query($query);
	}

	public function insert_proformat($input)
	{
		$this->insert_pro_mere($input);
		$this->insert_pro_fille($input);
	}

	public function insert($input) {
		$this->verifier_pro($input);
		$this->insert_proformat($input);
	}
    public function getDetails($bon)
    {
        $query = $this->db->get_where("vDetailsPro", array("proforma" => $bon));
        return $query->result_array();
    }

    public function getPro($bon)
    {
        $query = $this->db->get_where("vPro", array("id" => $bon));
        return $query->row_array();
    }
    public function getClient($id)
    {
        $query = $this->db->get_where("clients", array("idClient" => $id));
        return $query->row_array();
    }
    public function get_list_proformat()
    {
        $sql = "SELECT * from proforma order by id desc limit 5";
        $query = $this->db->query($sql);
        return $query->result_array();
    }
}
