-- use compta;

drop table detailsFacture;
drop table facture;
create table clients (
    idClient int primary key auto_increment,
    nom varchar(30) not null,
    localisation varchar(30) not null
);

create table facture (
    idFacture int primary key auto_increment,
    numero varchar(20) not null,
    dateFacture date,
    reference varchar(30),
    idClient int,
    remise real default 0,
    foreign key (idClient) references clients(idClient)
);

create table detailsFacture (
    idFacture int,
    idProduit int,
    idUnite int, 
    quantite real,
    prixUnitaire real,
    reduction real default 0,
    foreign key (idFacture) references facture(idFacture),
    foreign key (idProduit) references produits (idProduit),
	foreign key (idUnite) references unites (idUnite)
);

create view vDetailsFacture as
select detailsFacture.*,
	produits.nom as produit,
	unites.code as unite,
	prixUnitaire * quantite *(1 - reduction / 100) as total
from detailsFacture,
	produits,
	unites
where detailsFacture.idProduit = produits.idProduit
	and unites.idUnite = detailsFacture.idUnite;
create view calculFacture as
select idFacture,
	sum(total) as total
from vDetailsFacture
group by idFacture;
create view vFacture as
select facture.*,
	total,
	total *(1 - remise / 100) as ht,
	total *(1 - remise / 100) *(1.2) as ttc
from facture,
	calculFacture
where facture.idFacture = calculFacture.idFacture;
create view vListFacture as select facture.*, nom, localisation from facture, clients
where clients.idClient = facture.idClient;

create table stockProduit (
    idProduit int,
    methode int not null,
    foreign key (idProduit) references produits (idProduit)
);

create table entree (
    id int primary key auto_increment,
    idProduit int not null,
    quantite real not null,
    valeurUnitaire real not null,
    restant real not null,
    dateEntree date not null,
    foreign key (idProduit) references produits (idProduit)
);

create table sortie (
    id int primary key auto_increment,
    idProduit int not null,
    quantite real not null,
    valeurUnitaire real not null,
    dateSortie date not null,
    foreign key (idProduit) references produits (idProduit)
);

INSERT INTO `entree` (`id`, `idProduit`, `quantite`, `valeurUnitaire`, `restant`, `dateEntree`) VALUES ('1', '1', '3', '1000', '3', '2021-02-11'), ('2', '1', '4', '1500', '4', '2021-02-12');

INSERT INTO `stockproduit` (`idProduit`, `methode`) VALUES ('1', '1'), ('2', '2');

create view vFifo as select * from entree where restant > 0 order by dateEntree asc;

create view vLifo as select * from entree where restant > 0 order by dateEntree desc;

-- create view stock as select * from entree where restant > 0;

create or replace view vStock1 as select entree.*, methode, valeurUnitaire*restant as valeur from entree, stockProduit where entree.idProduit = stockProduit.idProduit;

create or replace view vStock2 as select idProduit, sum(restant) as restant, sum(valeur) as valeur from vStock1 where restant > 0 and methode != 3 group by idProduit;

create or replace view vStockC as select vStock1.idProduit, sum(vStock1.quantite) - sum(sortie.quantite) as restant, sum(valeur) as valeur  from vStock1, sortie where methode = 3 and vStock1.idProduit = sortie.idProduit group by vStock1.idProduit;

create or replace view vStockFinal as select * from vStock2 union select * from vStockC;

create or replace view stock as select produits.idProduit,produits.nom, restant, valeur from produits join vStockFinal on produits.idProduit = vStockFinal.idProduit;

create or replace view v_produitStock as select produits.* from produits, stockproduit where produits.idProduit = stockproduit.idProduit;

INSERT INTO `produits` (`idProduit`, `idType`, `nom`) VALUES ('4', '1', 'cahier'), ('5', '1', 'stylo'), ('6', '1', 'chaise'), ('7', '1', 'tableau blanc'), ('8', '1', 'marqueur');

INSERT INTO `stockproduit` (`idProduit`, `methode`) VALUES ('3', '2'), ('5', '1');

create or replace view idrecFin as select max(id) as id from bonreception;