drop database compta;
create database compta;
use compta;
create table types (
	idType int primary key,
	nom varchar(20)
);
create table devise (
	id int primary key,
	nom varchar(20) not null
);
create table cours (
	idDevise1 int,
	idDevise2 int,
	taux real,
	foreign key (idDevise1) references devise (id),
	foreign key (idDevise2) references devise (id)
);
create table produits (
	idProduit int primary key,
	idType int,
	nom varchar(20),
	foreign key (idType) references types (idType)
);
create table unites (
	idUnite int primary key,
	code varchar(10)
);
create table fournisseurs (
	idFournisseur int primary key,
	nom varchar (30),
	localisation varchar(20)
);
create table bon (
	id int primary key auto_increment,
	numero varchar(11),
	dateBon date,
	reference varchar(30),
	idFournisseur int,
	remise real,
	foreign key (idFournisseur) references fournisseurs (idFournisseur)
);
create table details (
	bon int,
	idProduit int,
	idUnite int,
	quantite real,
	prixUnitaire real,
	reduction real,
	foreign key (bon) references bon(id),
	foreign key (idProduit) references produits (idProduit),
	foreign key (idUnite) references unites (idUnite)
);
create table services (
	idService int primary key auto_increment,
	nom varchar(30)
);
create table demandeAchat (
	idDemandeAchat int primary key auto_increment,
	idService int,
	idProduit int,
	qt int,
	daty date,
	foreign key (idService) references services(idService),
	foreign key (idProduit) references produits (idProduit)
);
insert into demandeAchat (idDemandeAchat, idService, idProduit, qt, daty) values 
(null,1,1,5,'2021-02-05'),
(null,1,2,2,'2021-02-05');

create view vdemandeAchat as select demandeAchat.idDemandeAchat, demandeAchat.idService, 
		services.nom as services, demandeAchat.idProduit, 
		produits.nom as produits, demandeAchat.qt, demandeAchat.daty 
	from services join demandeAchat on services.idService=demandeAchat.idService
	join produits on produits.idProduit=demandeAchat.idProduit;

insert into services (idService, nom) values 
(null, 'Logistique'),
(null, 'Comptabilite'),
(null, 'Communication');

INSERT INTO `unites` (`idUnite`, `code`)
VALUES ('1', 'kg'),
	('2', 'l');
INSERT INTO `types` (`idType`, `nom`)
VALUES ('1', 'type1'),
	('2', 'type2');
INSERT INTO `produits` (`idProduit`, `idType`, `nom`)
VALUES ('1', '1', 'produit1'),
	('2', '2', 'produit2');
INSERT INTO `fournisseurs` (`idFournisseur`, `nom`, `localisation`)
VALUES ('1', 'fournisseur1', 'localisation f1'),
	('2', 'fournisseur2', 'localisqtion f2');
create view vDetails as
select details.*,
	produits.nom as produit,
	unites.code as unite,
	prixUnitaire * quantite *(1 - reduction / 100) as total
from details,
	produits,
	unites
where details.idProduit = produits.idProduit
	and unites.idUnite = details.idUnite;
create view calculBon as
select bon,
	sum(total) as total
from vDetails
group by bon;
create view vBon as
select bon.*,
	total,
	total *(1 - remise / 100) as ht,
	total *(1 - remise / 100) *(1.2) as ttc
from bon,
	calculBon
where bon.id = calculBon.bon;

create view vReception as  SELECT bonreception.*, nom, localisation FROM `bonreception` join fournisseurs  on bonreception.idfournisseur = fournisseurs.idfournisseur;
create view idBonFin as select coalesce(max(id), 0) id from bon;
create view idProFin as select coalesce(max(id), 0) id from proforma;

create view vLifo as
	select * from entree where restant > 0 order by dateEntree desc ;