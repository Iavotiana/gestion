

create table proforma (
	id int primary key auto_increment,
	numero varchar(10),
	dateProforma date,
	transport varchar(30),
	datePrevue date,
	idClient int,
	remise real,
	foreign key (idClient) references clients (idClient)
);


create table detailsPro (
	proforma int,
	idProduit int,
	idUnite int,
	quantite real,
	prixUnitaire real,
	reduction real,
	foreign key (proforma) references proforma(id),
	foreign key (idProduit) references produits (idProduit),
	foreign key (idUnite) references unites (idUnite)
);

create view vDetailsPro as
select detailsPro.*,
	produits.nom as produit,
	unites.code as unite,
	prixUnitaire * quantite *(1 - reduction / 100) as total
from detailsPro,
	produits,
	unites
where detailsPro.idProduit = produits.idProduit
	and unites.idUnite = detailsPro.idUnite;


create view calculPro as
select proforma,
	sum(total) as total
from vDetailsPro
group by proforma;

create view vPro as
select proforma.*,
	total,
	total *(1 - remise / 100) as ht,
	total *(1 - remise / 100) *(1.2) as ttc
from proforma,
	calculPro
where proforma.id = calculPro.proforma;

create view vListPro as select proforma.*, nom, localisation from
 proforma, clients where proforma.idClient = clients.idClient;