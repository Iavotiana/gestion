
create table services (
	idService int primary key auto_increment,
	nom varchar(30)
);
insert into services (idService, nom) values 
(null, 'Logistique'),
(null, 'Comptabilite'),
(null, 'Communication');

create table demandeAchat (
	idDemandeAchat int primary key auto_increment,
	idService int,
	idProduit int,
	qt int,
	daty date,
	etat int,
	foreign key (idService) references services(idService),
	foreign key (idProduit) references produits (idProduit)
);
insert into demandeAchat (idDemandeAchat, idService, idProduit, qt, daty, etat) values 
(null,1,1,5,'2021-02-05',0),
(null,1,2,2,'2021-02-05',0);

create or replace view vdemandeAchat as select demandeAchat.idDemandeAchat, demandeAchat.idService, 
		services.nom as services, demandeAchat.idProduit, 
		produits.nom as produits, demandeAchat.qt, demandeAchat.daty,
		demandeAchat.etat
	from services join demandeAchat on services.idService=demandeAchat.idService
	join produits on produits.idProduit=demandeAchat.idProduit where etat=0;

create table DemandeProformat (
	idProformat int primary key auto_increment,
	daty date
);
insert into demandeProformat (idProformat, daty) values 
(null, '2021-02-05'),
(null, '2021-02-06');

create table detailsProformat (
	idDetail int primary key auto_increment,
	idProformat int,
	idProduit int,
	qt int,
	daty date,
	foreign key (idProformat) references demandeProformat(idProformat),
	foreign key (idProduit) references produits (idProduit)
);
insert into detailsProformat (idDetail, idProformat,idProduit,qt,daty) values
(null,1,1,3,'2021-02-05'),
(null,1,2,3,'2021-02-05');

create or replace view vProformat1 as select detailsProformat.*, produits.nom 
from detailsProformat join produits on produits.idProduit=detailsProformat.idProduit order by idProformat;

create view vProformat as select detailsProformat.idProformat,detailsProformat.idProduit,sum(detailsProformat.qt)
 as qt, min(detailsProformat.daty) as daty,produits.nom 
  from detailsProformat join produits on produits.idProduit=detailsProformat.idProduit 
  group by idProduit,idProformat  order by idProformat;


create or replace view vDemandeProformat as select idProduit, produits, sum(qt) as qt, min(daty) as daty 
from vdemandeAchat group by idProduit;

create table demandeProfFournisseur (
	id int primary key auto_increment,
	idProformat int,
	idFournisseur int,
	foreign key (idProformat) references demandeProformat(idProformat),
	foreign key (idFournisseur) references fournisseurs (idFournisseur)
);
insert into demandeProfFournisseur (id,idProformat,idFournisseur) value 
(null,1,1);

 create view vDemandeProfFournisseur as 
 select demandeProfFournisseur.id, demandeProformat.idProformat,demandeProformat.daty, fournisseurs.*
 from demandeProformat join demandeProfFournisseur 
 on demandeProfFournisseur.idProformat = demandeProformat.idProformat
 join fournisseurs on fournisseurs.idFournisseur = demandeProfFournisseur.idFournisseur order by demandeProfFournisseur.id;
 

