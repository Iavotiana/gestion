use compta;

create table BonReception (
    id int primary key auto_increment,
    idFournisseur int,
    dateReception date,
    foreign key (idFournisseur) references fournisseurs(idFournisseur)
);

create table DetailsReception (
    id int primary key auto_increment,
    idBonReception int,   
    idProduit int, 
    quantite int,
    foreign key (idBonReception) references BonReception(id),
    foreign key (idProduit) references produits(idProduit)
);