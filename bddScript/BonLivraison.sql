-- use compta;

create table BonLivraison (
    id int primary key auto_increment,
    idClient int,
    dateLivraison date,
    foreign key (idClient) references clients(idClient)
);

create table DetailsLivraison (
    id int primary key auto_increment,
    idBonLivraison int,   
    idProduit int, 
    quantite int,
    foreign key (idBonLivraison) references BonLivraison(id),
    foreign key (idProduit) references produits(idProduit)
);

insert into clients(nom, localisation) values('client1', 'localisation1'), ('client2', 'localisation2');
